# Slipper Trip

## UNSW COMP9900 Project

#### Group Name: XWEX-PUBG

#### Group Members: Li Xu(z5136365), Bowen Fan(z5137961),  Jingchang Wang(z5102326), Yue Xiong(z5109140)



### ***All project env/src packages are described in README.md of each directories.***

## Screenshots of website
* Send active email and verify the email address
![](screenshots/1.png)
* homepage & search
![](screenshots/2.png)
* search results
![](screenshots/3.png)
* AI searching results
![](screenshots/4.png)
* accommodation detail
![](screenshots/5.png)
* reserve an accommodation
![](screenshots/6.png)
* user account
![](screenshots/7.png)
* login
![](screenshots/8.png)
* reserved list
![](screenshots/9.png)
* sign up
![](screenshots/10.png)
* user profile
![](screenshots/11.png)