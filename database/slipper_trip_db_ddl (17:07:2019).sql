create table user_user
(
    id           int auto_increment
        primary key,
    password     varchar(128) not null,
    last_login   datetime(6)  null,
    is_superuser tinyint(1)   not null,
    email        varchar(255) not null,
    username     varchar(30)  not null,
    phone        varchar(10)  not null,
    is_active    tinyint(1)   not null,
    is_admin     tinyint(1)   not null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);



create table st_user_kw
(
    id       int auto_increment
        primary key,
    keyword1 varchar(2) not null,
    keyword2 varchar(2) not null,
    keyword3 varchar(2) not null,
    user_id  int        not null,
    constraint st_user_kw_user_id_834f76e4_fk_user_user_id
        foreign key (user_id) references user_user (id)
);





create table st_acco_info
(
    id           int auto_increment
        primary key,
    keyword1     varchar(2)     not null,
    keyword2     varchar(2)     not null,
    keyword3     varchar(2)     not null,
    create_time  datetime(6)    not null,
    update_time  datetime(6)    not null,
    is_delete    tinyint(1)     not null,
    title        varchar(100)   not null,
    addr_street  varchar(100)   not null,
    addr_city    varchar(30)    not null,
    people       int            not null,
    start_date   date           not null,
    end_date     date           not null,
    price        int            not null,
    acco_score   decimal(2, 1)  not null,
    beds         int            not null,
    bedrooms     int            not null,
    carparks     int            not null,
    latitude     decimal(10, 7) null,
    longtitude   decimal(10, 7) null,
    host_user_id int            not null,
    constraint st_acco_info_host_user_id_6be70e13_fk_user_user_id
        foreign key (host_user_id) references user_user (id)
);



create table st_order
(
    id            int auto_increment
        primary key,
    create_time   datetime(6)  not null,
    update_time   datetime(6)  not null,
    is_delete     tinyint(1)   not null,
    u_title       varchar(1)   not null,
    u_lastname    varchar(30)  not null,
    u_firstname   varchar(30)  not null,
    u_addr_street varchar(100) not null,
    u_addr_city   varchar(30)  not null,
    u_people      int          not null,
    u_telephone   varchar(13)  not null,
    u_email       varchar(254) not null,
    days          int          not null,
    total_price   int          not null,
    order_status  tinyint(1)   not null,
    acco_id       int          not null,
    user_id       int          not null,
    constraint st_order_acco_id_c612e39c_fk_st_acco_info_id
        foreign key (acco_id) references st_acco_info (id),
    constraint st_order_user_id_02049124_fk_user_user_id
        foreign key (user_id) references user_user (id)
);



create table st_acco_ficilities
(
    id              int auto_increment
        primary key,
    air_conditioner tinyint(1) not null,
    tv              tinyint(1) not null,
    wifi            tinyint(1) not null,
    telephone       tinyint(1) not null,
    shampoo         tinyint(1) not null,
    body_wash       tinyint(1) not null,
    freezer         tinyint(1) not null,
    acco_id         int        not null,
    constraint st_acco_ficilities_acco_id_1bd74bc8_fk_st_acco_info_id
        foreign key (acco_id) references st_acco_info (id)
);




create table st_acco_pictures
(
    id           int auto_increment
        primary key,
    picture_addr varchar(100) not null,
    acco_id      int          not null,
    constraint st_acco_pictures_acco_id_16899129_fk_st_acco_info_id
        foreign key (acco_id) references st_acco_info (id)
);




create table st_acco_reserved_date
(
    id       int auto_increment
        primary key,
    date_in  date not null,
    date_out date not null,
    order_id int  not null,
    constraint st_acco_reserved_date_order_id_f0dbfde3_fk_st_order_id
        foreign key (order_id) references st_order (id)
);




create table st_acco_reviews
(
    id       int auto_increment
        primary key,
    review   longtext      not null,
    rate     decimal(2, 1) not null,
    order_id int           not null,
    constraint st_acco_reviews_order_id_8b9fba98_fk_st_order_id
        foreign key (order_id) references st_order (id)
);




create table st_saved
(
    id          int auto_increment
        primary key,
    save_status tinyint(1) not null,
    acco_id     int        not null,
    user_id     int        not null,
    constraint st_saved_acco_id_83d13618_fk_st_acco_info_id
        foreign key (acco_id) references st_acco_info (id),
    constraint st_saved_user_id_a78f854a_fk_user_user_id
        foreign key (user_id) references user_user (id)
);



create table st_sys_config
(
    id           int auto_increment
        primary key,
    config_name  varchar(20) not null,
    config_index varchar(20) not null
);






