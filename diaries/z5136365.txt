************************************************************
Week 1 --09/06/2019
Done:
    - Group formed.
    - I created the GitHub Repository for our course team.
    - Through discussion, we decided to choose the third project topic, Accommodation Web Portal.
    - Confirmed the front end is developed using the React framework and HTML, JS, and CSS languages.
    - The backend is developed using the Django framework and the Python language.
    - Use MySQL DB.
    - Develop a project development plan, process and framework.
    - Start writing project proposal documents.
    - Review the Python Django framework.

------------------------------------------------------------

TODO:
    - Make logical process diagram.
    - Make framework process diagram.
    - Go on writing project proposal documents.
************************************************************

************************************************************
Week 2 --15/06/2019
Done:
    - Through discussion, we confirmed more details about the project development.
    - Made logical process diagram.
    - Made framework process diagram.
    - Previewed the referenced websites.
    - Changed the GitHub team onto course official team repository.
    - Chose GitKraken to be the graphical git manager.
    - Split the tasks to team members and used GitKraken Glo boards to manage tasks.

------------------------------------------------------------

technical obstacles:
    - No suitable data source, or datasets.
    # But now we tried to use crawler on the referenced websites.

------------------------------------------------------------

TODO:
    - Design MySQL DB on AWS.
    - Establish backend framework.
************************************************************

************************************************************
Week3 --22/06/2019
Done:
    - Finished the proposal with team members together.
    - Designed and fixed MySql database on AWS server.
    - Partly established the backend framework.

------------------------------------------------------------

TODO:
    - Finish the backend framework establish.
    - Add the redis and celery in the project.
    - Start to make progress on APIs, Eg: Sign in/ Sign up/ Sign out
************************************************************

************************************************************
Week4 --30/06/2019
Done:
    - Almost finish the backend framework establish.
    - Add the redis database and celery in the project.
    - Already done the sign in/ sign up/ sign out function.
    - Already done the function of create superuser admin management.

------------------------------------------------------------

TODO:
    - Go on writing the APIs of backend.
    - Go on optimizing db design.
************************************************************

************************************************************
Week5 --06/07/2019
Done:
    - Improved sign in/ sign up/ sign out APIs.
    - Changed DB server from AWS to IFAST.
    - Set up python environment and Django Framework on AWS server.

------------------------------------------------------------

TODO:
    - Go on writing  backend APIs.
    - Design and draw the LOGO.
************************************************************

************************************************************
Week6 --13/07/2019
Done:
    - Finished backend accommodations ADD/DELETE/ALTER/SEARCH APIs.
    - Optimised the product proposal and UI design of the project.
    - Finished accommodations SAVE/SEARCH APIs.
    
------------------------------------------------------------

TODO:
    - Start to draft the report.
    - Finish the main functional APIs on backend.
************************************************************

************************************************************
Week7 --19/07/2019
Done:
    - Update hotels APIs.
    - Add order related APIs.
    - Finish almost all of the backend APIs.
    - Improve some of the older APIs.

------------------------------------------------------------
TODO:
    - Combine the APIs with frontend functions.
    - Deploy backend APIs onto AWS server.
    - Solve the problem of delay when call the API.
************************************************************

************************************************************ 
Week8 --28/07/2019
Done:
    - Improved backend APIs.
    - Filled the dataset into database, and change some APIs to adapt with the dataset.
------------------------------------------------------------
TODO:
    - Going on improving functional APIs.
    - Prepare demo and presentation.
    - Finish final report.
************************************************************

************************************************************
Week9 --04/08/2019
Done:
    - Improved backend APIs.
    - Add machinelearning algorithms into backend project.
    - Fix bugs and improved for demo.
------------------------------------------------------------
TODO:
    - Demo.
    - Presentation.
    - Finish report.
************************************************************

************************************************************
Week10 --10/08/2019
Done:
    - Demo.
    - Presentation.
    - Finish report.
************************************************************

************************************************************
Over!
    
    
    
    
