import React from "react";
import "./App.css";
// import jwtDecode from "jwt-decode";
import { Route, Switch, Redirect } from "react-router-dom";
import { Layout } from "antd";
import HomePage from "./Pages/public/HomePage";
import LoginPage from "./Pages/public/LoginPage";
import SignUpPage from "./Pages/public/SignupPage";
import NotFoundPage from "./Pages/public/NotFoundPage";
import NavBar from "./components/common/navBar";
import Account from "./Pages/account/account";
import MyProperty from "./Pages/account/myproperty";
import accommodation from "./Pages/accommodation/accommodation";
import accommodationdetails from "./Pages/accommodation/accommodationdetails";
import Saved from "./Pages/account/saved";
import Reserved from "./Pages/account/reserved";
import UserProfile from "./Pages/account/UserProfile";
import PostPage from "./Pages/account/postpage";
import Logout from "./Pages/account/logout";
import SignupInfo from "./Pages/account/signupinfo";
import OrderDetailers from "./Pages/account/orderdetails";
import ValidationSuccess from "./Pages/account/emailvalidationsuccess";
import EditPostForm from "./components/editPostForm";
import EditUserprofile from "./Pages/account/editUserProfile";
import RevidewRate from "./components/reviewRate";

class App extends React.Component {
  state = { username: "" };

  componentDidMount() {
    try {
      // const user = localStorage.getItem("token");
      const username = localStorage.getItem("username");

      // const { username } = jwtDecode(user);
      this.setState({ username });
    } catch (ex) {}
  }
  componentDidUpdate() {
    window.scrollTo(0, 0);
  }

  render() {
    const { Content, Footer } = Layout;

    return (
      <Layout className="layout">
        <div>
          <NavBar username={this.state.username} />
        </div>

        <Content>
          <div style={{ paddingTop: 60 }}>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route
                exact
                path="/orderdetails/:id"
                component={OrderDetailers}
              />
              <Route
                path="/acclistdetails/:id/:duration?/"
                component={accommodationdetails}
              />
              <Route path="/acclist/:query?" component={accommodation} />
              <Route path="/login/:fromurl?" component={LoginPage} />
              <Route path="/logout" component={Logout} />
              <Route path="/signup" component={SignUpPage} />
              <Route path="/signupinfo" component={SignupInfo} />
              <Route path="/account" component={Account} />
              <Route path="/notfound" component={NotFoundPage} />
              ValidationSuccess
              <Route path="/validationsuccess" component={ValidationSuccess} />
              <Route
                onUpdate={() => window.scrollTo(0, 0)}
                path="/property"
                component={MyProperty}
              />
              <Route
                onUpdate={() => window.scrollTo(0, 0)}
                path="/saved"
                component={Saved}
              />
              <Route
                onUpdate={() => window.scrollTo(0, 0)}
                path="/reserved"
                component={Reserved}
              />
              <Route
                onUpdate={() => window.scrollTo(0, 0)}
                path="/userprofile"
                component={UserProfile}
              />
              <Route path="/edituserprofile/:id" component={EditUserprofile} />
              <Route
                onUpdate={() => window.scrollTo(0, 0)}
                path="/postform"
                component={PostPage}
              />
              <Route path="/reviewrate/:id" component={RevidewRate} />
              <Route path="/editpostform/:id" component={EditPostForm} />
              <Redirect to="/notfound" />
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          slipperTrip ©2019 Created by slipper Team
        </Footer>
      </Layout>
    );
  }
}

export default App;
