import React, { Component } from "react";
import dataSource from "../Pages/public/address";
import QueueAnim from "rc-queue-anim";
import moment from "moment";

import { Form, Row, Button, AutoComplete, DatePicker, InputNumber } from "antd";
const { RangePicker } = DatePicker;

class hform extends Component {
  state = {};

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const queryData = {
          addr_city: values["addr_city"],
          start_date: values["timeRange"][0].format("YYYY-MM-DD"),
          end_date: values["timeRange"][1].format("YYYY-MM-DD"),
          people: values["people"]
        };
        console.log("query", queryData);
        this.props.onSubmit(queryData);
      }
    });
  };
  disabledDate = current => {
    // Can not select days before today and today
    // console.log("endofday", moment().endOf("day"));
    return current < moment().subtract(1, "days");
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        }
      }
    };

    return (
      <Form
        layout="vertical"
        onSubmit={this.handleSubmit}
        style={{ width: 380, padding: 20 }}
      >
        <QueueAnim type="right" delay={900}>
          <Form.Item key="11" label="Destination" style={{ margin: 0 }}>
            {getFieldDecorator("addr_city", {
              initialValue: "Bondi Beach",
              rules: []
            })(
              <AutoComplete
                style={{ width: 360 }}
                size="large"
                dataSource={dataSource}
                placeholder="where..."
                filterOption={(inputValue, option) =>
                  option.props.children
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              />
            )}
          </Form.Item>
          <Form.Item key="1" label="Check-In & Check-out" style={{ margin: 0 }}>
            {getFieldDecorator("timeRange", {
              rules: []
            })(
              <RangePicker
                disabledDate={this.disabledDate}
                size="large"
                style={{ width: 360 }}
              />
            )}
          </Form.Item>
          {/* <Form.Item key="2" label="Check-Out" style={{ margin: 0 }}>
            {getFieldDecorator("end_date", {
              rules: []
            })(<DatePicker size="large" style={{ width: 360, margin: 0 }} />)}
          </Form.Item> */}
          <Form.Item key="3" label="Guests" style={{ margin: 0 }}>
            {getFieldDecorator("people", {
              initialValue: 1,
              rules: []
            })(<InputNumber min={1} size="large" />)}
          </Form.Item>
          <Row type="flex" justify="end">
            <Form.Item key="4" {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" size="large">
                Search
              </Button>
            </Form.Item>
          </Row>
        </QueueAnim>
      </Form>
    );
  }
}

const HomepageForm = Form.create({ name: "HomepageForm" })(hform);

export default HomepageForm;
