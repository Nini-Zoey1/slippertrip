import React from "react";
import GoogleMapReact from "google-map-react";
// import { Icon } from "antd";
import Octicon, { Pin } from "@githubprimer/octicons-react";

const MapSection = ({ lat, lng }) => {
  const _lat = parseFloat(lat);
  const _lng = parseFloat(lng);
  return _lat && _lng ? (
    <div style={{ height: 550, width: 380 }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyCBVQWFkkbubt8DGOYPE5MRrnd_esAYFts" }}
        defaultCenter={{
          lat: _lat,
          lng: _lng
        }}
        defaultZoom={18}
      >
        <img
          alt=""
          src="https://img.icons8.com/color/48/000000/map-pin.png"
          lat={_lat}
          lng={lng}
        />
        {/* <Octicon icon={Pin} size="large" lat={_lat} lng={lng} /> */}
      </GoogleMapReact>
    </div>
  ) : (
    <div />
  );
};

export default MapSection;
