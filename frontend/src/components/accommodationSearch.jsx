import React, { Component } from "react";
import { Row, Form, AutoComplete, DatePicker, InputNumber, Button } from "antd";
import QueueAnim from "rc-queue-anim";

import dataSource from "../Pages/public/address";
import moment from "moment";
const { RangePicker } = DatePicker;

class innerAccommodationSearch extends Component {
  state = {};
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const queryData = {
          addr_city: values["addr_city"],
          start_date: values["timeRange"][0].format("YYYY-MM-DD"),
          end_date: values["timeRange"][1].format("YYYY-MM-DD"),
          people: values["people"]
        };
        console.log("query", queryData);
        this.props.onSubmit(queryData);
      }
    });
  };
  disabledDate = current => {
    // Can not select days before today and today
    // console.log("endofday", moment().endOf("day"));
    return current < moment().subtract(1, "days");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { start_date, end_date, addr_city, people } = this.props.defaultQuery;
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        }
      }
    };

    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <QueueAnim type="left" delay={600}>
          <Form.Item key="11" label="Destination">
            {getFieldDecorator("addr_city", {
              initialValue: addr_city,
              rules: [
                {
                  required: true,
                  message: "Please input your Destination!"
                }
              ]
            })(
              <AutoComplete
                style={{ width: 120 }}
                //   size="large"
                dataSource={dataSource}
                placeholder="where..."
                filterOption={(inputValue, option) =>
                  option.props.children
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              />
            )}
          </Form.Item>
          <Form.Item key="1" label="Date">
            {getFieldDecorator("timeRange", {
              initialValue: [moment(start_date), moment(end_date)],

              rules: [
                {
                  required: true,
                  message: "Please input your Date!"
                }
              ]
            })(
              <RangePicker
                disabledDate={this.disabledDate}
                //   size="large"
                style={{ width: 240 }}
              />
            )}
          </Form.Item>
          {/* <Form.Item key="2" label="Check-Out" >
                {getFieldDecorator("end_date", {
                  rules: []
                })(<DatePicker size="large" style={{ width: 360, margin: 0 }} />)}
              </Form.Item> */}
          <Form.Item key="3" label="Guests">
            {getFieldDecorator("people", {
              initialValue: people,
              rules: [
                {
                  required: true,
                  message: "Please input your Guests!"
                }
              ]
            })(<InputNumber min={1} style={{ width: 50 }} />)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              Search
            </Button>
          </Form.Item>
        </QueueAnim>
      </Form>
    );
  }
}
const AccommodationSearch = Form.create({ name: "HomepageForm" })(
  innerAccommodationSearch
);

export default AccommodationSearch;
