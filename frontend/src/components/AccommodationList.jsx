import React from "react";
import { List } from "antd";
import AccommodationListItem from "./accommodationListItem";
import InfiniteScroll from "react-infinite-scroller";
import QueueAnim from "rc-queue-anim";

const AccommodationList = ({
  items,
  duration,
  handleCurrentStop,
  handleLiked,
  likedList,
  handleLikedListAdd,
  handleLikedListRemove,
  handleCheckLikedOrNot
}) => {
  const logCurrentPage = page => {
    handleCurrentStop(page);
  };
  // console.log("url111", `/acclistdetails/${2}/${duration}/`);

  return (
    <div
      style={{
        // overflow: "auto",
        // height: 500,
        border: 1,
        borderStyle: "solid",
        borderColor: "#e8e8e8",
        borderRadius: 4
      }}
    >
      <QueueAnim delay={300} type="top">
        {/* <AccommodationListItem key={1} item={item[0]} />
        <AccommodationListItem key={2} item={item[1]} /> */}
        {/* <AccommodationListItem key={3} item={item[2]} />
        <AccommodationListItem key={4} item={item[3]} />
        <AccommodationListItem key={5} item={item[4]} /> */}
      </QueueAnim>
      {/* <InfiniteScroll
        pageStart={0}
        initialLoad={false}
        loadMore={logCurrentPage}
        hasMore={false}
        useWindow={false}

        // loader={<div className="loader" key={0}>Loading ...</div>}
      > */}
      <List
        key="1aq"
        pagination={{
          onChange: page => {
            // console.log(page);
          },
          pageSize: 5
        }}
        itemLayout="horizontal"
        dataSource={items}
        renderItem={item => (
          <List.Item>
            {/* <List.Item.Meta
            avatar={
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            }
            title={<a href="https://ant.design">{item.title}</a>}
            description="Ant Design, a design language for background applications, is refined by Ant UED Team"
          /> */}
            <AccommodationListItem
              duration={duration}
              item={item}
              handleLiked={() => handleLiked(item.id)}
              isLiked={likedList.includes(item.id)}
              // handleCheckLikedOrNot={handleCheckLikedOrNot}
              // likedList={likedList}
              // handleLikedListAdd={handleLikedListAdd}
              // handleLikedListRemove={handleLikedListRemove}
            />
          </List.Item>
        )}
      />
      {/* </InfiniteScroll> */}
    </div>
  );
};

export default AccommodationList;
