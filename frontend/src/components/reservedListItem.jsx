import React, { Component } from "react";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import moment from "moment";
class ReservedListItem extends Component {
  render() {
    const { acco_info, order_detail, host_user } = this.props.item;
    // console.log("ReservedListItem acco_info ", acco_info);
    // console.log("ReservedListItem order_detail ", order_detail);
    // console.log("ReservedListItem host_user ", host_user);
    const today = new Date();
    return (
      <Row
        style={{
          margin: 20,
          border: "2px solid rgba(129, 71, 14,0.5)",
          padding: "25px 10px 0 5px",
          borderRadius: "10px",
          height: "230px"
        }}
        gutter={20}
        type="flex"
        justify="space-around"
        align="top"
      >
        <Col sm={24} md={8} lg={6}>
          <img
            style={{ width: "100%", height: 170 }}
            src={acco_info.pic_url}
            alt=""
          />
        </Col>
        <Col sm={24} md={12} lg={12}>
          <Row>
            <Col>
              <Link to={`orderdetails/${order_detail.id}`}>
                <h5>{acco_info.title}</h5>
                {/* <h5>{`/acclistdetails/${item.id}/${this.props.duration}/`}</h5> */}
              </Link>
            </Col>
          </Row>

          <Row>
            <Col sm={24} md={24}>
              <p>
                address: <span>{acco_info.addr_city}</span>
              </p>
            </Col>
            <Col sm={24} md={24}>
              <p>
                check in: <span>{order_detail.date_in.slice(0, 10)}</span>
              </p>
              <p>
                check out: <span>{order_detail.date_out}</span>
              </p>
            </Col>
            {/* <Col sm={24} md={24}>
              <p>
                reviews_number: <span>{item.reviews_number}</span>
              </p>
            </Col> */}
          </Row>

          <Row>
            <Col sm={24} md={24} style={{ color: "brown", fontSize: 17 }}>
              <p>
                {" "}
                <strong>Price: ${order_detail.total_price} </strong>
              </p>
            </Col>
          </Row>
        </Col>

        <Col sm={24} md={4} lg={2}>
          <Button
            style={{ marginBottom: 20 }}
            type="primary"
            onClick={() => this.props.onDelete(order_detail.id)}
            disabled={moment(today).isAfter(moment(order_detail.date_in))}
          >
            Delete
          </Button>
          <Button
            type="primary"
            onClick={() => this.props.onReview(order_detail.id)}
            disabled={moment(today).isBefore(moment(order_detail.date_out))}
          >
            Review
          </Button>
        </Col>
      </Row>
    );
  }
}

export default ReservedListItem;
