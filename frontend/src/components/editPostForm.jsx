import React, { Component } from "react";
import QueueAnim from "rc-queue-anim";
import moment from "moment";
import {
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  InputNumber,
  Checkbox,
  Button,
  Spin
} from "antd";
import axios from "axios";
import { apiUrl } from "../config.json";

localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));

const plainOptions = [
  "air_conditioner",
  "tv",
  "wifi",
  "kitchen",
  "shampoo",
  "hair_dryer",
  "freezer"
];

class preEditPostForm extends Component {
  state = {
    item: {
      title: "aaaaa",
      addr_street: "2 Muller Lane",
      addr_city: "Mascot",
      people: 2,
      start_date: "2019-08-11",
      end_date: "2019-08-30",
      price: 400,
      beds: 2,
      bedrooms: 2,
      carparks: 1,
      description: "dddddddd",
      air_conditioner: "True",
      tv: "True",
      wifi: "True",
      telephone: "True",
      shampoo: "True",
      hair_dryer: "True",
      freezer: "True"
    },

    checkedList: [],
    indeterminate: true,
    checkAll: false,
    loading: false,
    preparing: false
  };
  componentDidMount() {
    const id = this.props.match.params.id;
    // console.log(id);
    this.setState({ preparing: true });
    axios.get(`http://127.0.0.1:8000/api/hotel/${id}/view/`).then(res => {
      //   console.log(res);
      this.setState({ item: res.data.data.acco_info }, () => {
        let defaultCheckedList = [];
        if (this.state.item.air_conditioner)
          defaultCheckedList.push("air_conditioner");
        if (this.state.item.tv) defaultCheckedList.push("tv");
        if (this.state.item.wifi) defaultCheckedList.push("wifi");
        if (this.state.item.telephone) defaultCheckedList.push("telephone");
        if (this.state.item.shampoo) defaultCheckedList.push("shampoo");
        if (this.state.item.hair_dryer) defaultCheckedList.push("hair_dryer");
        if (this.state.item.freezer) defaultCheckedList.push("freezer");
        this.setState({ checkedList: defaultCheckedList });
      });

      this.setState({ preparing: false });
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // console.log("Received values of form: ", values);
        // console.log(
        //   "Received values of range-picker: ",
        //   values["range-picker"][0].format("YYYY-MM-DD"),
        //   values["range-picker"][1].format("YYYY-MM-DD")
        // );
        // console.log(this.handleaminities(this.state.checkedList));
        const query = {
          ...this.handleaminities(this.state.checkedList),
          title: values.title,
          addr_street: values.addr_street,
          addr_city: values.addr_city,
          people: values.people,
          start_date: values["range-picker"][0].format("YYYY-MM-DD"),
          end_date: values["range-picker"][1].format("YYYY-MM-DD"),
          price: values.price,
          beds: values.beds,
          bedrooms: values.bedrooms,
          bathrooms: values.bathrooms,
          description: values.description
          // air_conditioner: "True",
          // tv: "True",
          // wifi: "True",
          // kitchen: "True",
          // shampoo: "True",
          // body_wash: "True",
          // hair_dryer: "True",
          // freezer: "True"
        };
        console.log("query", query);
        this.setState({ loading: true });
        axios
          .put(
            `http://127.0.0.1:8000/api/hotel/${
            this.props.match.params.id
            }/edit/`,
            query
          )
          .then(res => {
            // console.log(res);
            this.setState({ loading: false });
            // console.log(this.props);
            this.props.history.push("/property");
          })
          .catch(error => console.log(error));
      }
      // this.props.history.push("/property");
    });
  };
  handleaminities = alist => {
    let aminities = {
      air_conditioner: "False",
      tv: "False",
      wifi: "False",
      kitchen: "False",
      shampoo: "False",
      body_wash: "False",
      hair_dryer: "False",
      freezer: "False"
    };
    for (let i in alist) {
      aminities[alist[i]] = "True";
    }
    return aminities;
  };

  onChange = checkedList => {
    this.setState({
      checkedList,
      indeterminate:
        !!checkedList.length && checkedList.length < plainOptions.length,
      checkAll: checkedList.length === plainOptions.length
    });
  };

  onCheckAllChange = e => {
    this.setState({
      checkedList: e.target.checked ? plainOptions : [],
      indeterminate: false,
      checkAll: e.target.checked
    });
  };
  handleClick = () => {
    console.log(this.state.checkedList);
  };
  disabledDate = current => {
    // Can not select days before today and today
    // console.log("endofday", moment().endOf("day"));
    return current < moment().subtract(1, "days");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { RangePicker } = DatePicker;
    const { TextArea } = Input;
    const CheckboxGroup = Checkbox.Group;
    const { item } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
      }
    };
    const formGroupItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };
    const formNumberItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
      }
    };

    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 11
        }
      }
    };

    return this.state.preparing ? (

      <Row type="flex" justify="center" gutter={40}>
        <Col>
          <Spin size="large" />
        </Col>
      </Row>
    ) : (
        <div style={{
          background:
            "linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          backgroundSize: "cover",
          marginTop: "-60px",
          padding: 40
        }}>
          <Row type="flex" justify="center" style={{ marginTop: "50px" }} >
            <Col sm={24} md={18} style={{ borderRadius: 20 }}>
              <Form
                {...formItemLayout}
                onSubmit={this.handleSubmit}
                labelAlign="right"
                style={{
                  // maxWidth: 700,
                  backgroundColor: "rgba(245,247,250,0.9)",
                  borderRadius: 20,
                  padding: 30
                }}
              >
                <div />
                <Row gutter={40} style={{ marginBottom: 0 }}>
                  <Col xs={24} md={12}>
                    <QueueAnim type="bottom" delay={500}>
                      <Form.Item key="a" label="Property name">
                        {getFieldDecorator("title", {
                          initialValue: item.title,

                          rules: [
                            {
                              type: "string",
                              message: ""
                            },
                            {
                              required: true,
                              message: "Please input property title!"
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                      <Form.Item key="b" label="Address street">
                        {getFieldDecorator("addr_street", {
                          initialValue: item.addr_street,
                          rules: [
                            {
                              type: "string",
                              message: ""
                            },
                            {
                              required: true,
                              message: "Please input property street!"
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                      <Form.Item key="c" label="Address city">
                        {getFieldDecorator("addr_city", {
                          initialValue: item.addr_city,
                          rules: [
                            {
                              type: "string",
                              message: ""
                            },
                            {
                              required: true,
                              message: "Please input address city!"
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                      <Form.Item key="d" label="Start & end date">
                        {getFieldDecorator("range-picker", {
                          initialValue: [
                            moment(item.start_date),
                            moment(item.end_date)
                          ],

                          rules: [
                            {
                              type: "array",
                              required: true,
                              message: "Please select time!"
                            }
                          ]
                        })(<RangePicker disabledDate={this.disabledDate} />)}
                      </Form.Item>
                    </QueueAnim>
                  </Col>
                  <Col xs={24} md={12}>
                    <QueueAnim type="right" delay={500}>
                      <Form.Item
                        key="a"
                        {...formNumberItemLayout}
                        label="Perferred Price($)"
                      >
                        {getFieldDecorator("price", {
                          initialValue: item.price,
                          rules: [
                            {
                              type: "number",
                              message: "should be a number"
                            },
                            {
                              required: true,
                              message: "Please input perferred price !"
                            }
                          ]
                        })(<InputNumber min={0} />)}
                      </Form.Item>
                      <Form.Item key="b" {...formNumberItemLayout} label="Capacity">
                        {getFieldDecorator("people", {
                          initialValue: item.people,
                          rules: [
                            {
                              type: "number",
                              message: "should be a number"
                            },
                            {
                              required: true,
                              message: "Please input capacity!"
                            }
                          ]
                        })(<InputNumber min={0} />)}
                      </Form.Item>
                      <Form.Item key="c" {...formNumberItemLayout} label="Beds">
                        {getFieldDecorator("beds", {
                          initialValue: item.beds,
                          rules: [
                            {
                              type: "number",
                              message: "should be a number"
                            },
                            {
                              required: true,
                              message: "Please input No. of beds!"
                            }
                          ]
                        })(<InputNumber min={0} />)}
                      </Form.Item>

                      <Form.Item key="d" {...formNumberItemLayout} label="Bedrooms">
                        {getFieldDecorator("bedrooms", {
                          initialValue: item.bedrooms,
                          rules: [
                            {
                              type: "number",
                              message: "should be a number"
                            },
                            {
                              required: true,
                              message: "Please input No. of bedroomss!"
                            }
                          ]
                        })(<InputNumber min={0} />)}
                      </Form.Item>

                      <Form.Item
                        key="e"
                        {...formNumberItemLayout}
                        label="bathrooms"
                      >
                        {getFieldDecorator("bathrooms", {
                          initialValue: item.bathrooms,
                          rules: [
                            {
                              type: "number",
                              message: "should be a number"
                            },
                            {
                              required: true,
                              message: "Please input No. of carparks!"
                            }
                          ]
                        })(<InputNumber min={0} />)}
                      </Form.Item>
                    </QueueAnim>
                  </Col>
                </Row>
                <Row style={{ paddingRight: 20 }}>
                  <QueueAnim type="bottom">
                    <Form.Item key="a" {...formGroupItemLayout} label="Amenities">
                      <div>
                        <div
                          style={{
                            borderBottom: "2px solid #E9E9E9",
                            marginBottom: 5
                          }}
                        >
                          <Checkbox
                            indeterminate={this.state.indeterminate}
                            onChange={this.onCheckAllChange}
                            checked={this.state.checkAll}
                          >
                            Check all
                      </Checkbox>
                        </div>
                        <CheckboxGroup
                          options={plainOptions}
                          value={this.state.checkedList}
                          onChange={this.onChange}
                        />
                      </div>
                    </Form.Item>

                    <Form.Item key="b" {...formGroupItemLayout} label="Description">
                      {getFieldDecorator("description", {
                        initialValue: item.description,

                        rules: [
                          {
                            type: "string",
                            message: "should be a number"
                          },
                          {
                            required: true,
                            message: "Please input description!"
                          }
                        ]
                      })(<TextArea placeholder="a brief introduction..." />)}
                    </Form.Item>
                  </QueueAnim>
                </Row>
                <QueueAnim type="bottom" ease={["easeOutQuart"]}>
                  <Form.Item {...tailFormItemLayout}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={this.state.loading}
                    >
                      Submit
                </Button>
                  </Form.Item>
                </QueueAnim>
              </Form>{" "}
            </Col>
          </Row>
        </div>
      );
  }
}

const EditPostForm = Form.create({ name: "register" })(preEditPostForm);

export default EditPostForm;
