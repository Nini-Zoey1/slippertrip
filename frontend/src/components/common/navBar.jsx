import React from "react";
import "./Navbar.css";
import { Link } from "react-router-dom";
import QueueAnim from "rc-queue-anim";

//import FontAwesome from 'react-fontawesome';

class Home extends React.Component {
  render() {
    return (
      <div className="header">
        <h3 className="logo-text">
          <span style={{ color: "white" }}>Slipper</span>trip
        </h3>
        <div className="icon" />
        <nav>
          <ul className="nav">
            <QueueAnim delay={1000} type="bottom">
              <li key="home" className="nav-li">
                <Link to="/">Home</Link>
              </li>
              {this.props.username && (
                <li key="account" className="nav-li">
                  <Link to="/account">{this.props.username} 's Account</Link>
                </li>
              )}
              {this.props.username && (
                <li key="logout" className="nav-li">
                  <Link to="/logout">Logout</Link>
                </li>
              )}
              {/* <li className="nav-li">
              <Link to="/acclist">Accommodation</Link>
            </li> */}
              {!this.props.username && (
                <li
                  key="login"
                  className="nav-li"
                  // onClick={localStorage.setItem(
                  //   "fromurl",
                  //   // this.props.location.pathname
                  // )}
                >
                  <Link to="/login">Login</Link>
                </li>
              )}
            </QueueAnim>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Home;
