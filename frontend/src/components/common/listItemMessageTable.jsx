import React from "react";
import { Descriptions } from "antd";
import { Link } from "react-router-dom";

export default class ListItemMessageTable extends React.Component {
  onChange = e => {
    console.log("size checked", e.target.value);
    this.setState({
      size: e.target.value
    });
  };

  render() {
    const { accommodation } = this.props;
    return (
      <div>
        <Descriptions bordered title="" border size="default">
          <Descriptions.Item label="">
            <img style={{ width: "100%" }} src={accommodation.url} alt="" />
          </Descriptions.Item>
          <Descriptions.Item label="Name">
            <Link to={`/acclist/${accommodation.id}`}>
              <h3>{accommodation.title}</h3>
            </Link>
          </Descriptions.Item>
          <Descriptions.Item label="address">Address</Descriptions.Item>
          <Descriptions.Item label="amenities">amenities</Descriptions.Item>
          <Descriptions.Item label="Official">
            {accommodation.description}
          </Descriptions.Item>
          <Descriptions.Item label="Official">$60.00</Descriptions.Item>
          <Descriptions.Item label="Config Info">
            Data disk type: MongoDB
            <br />
            Database version: 3.4
            <br />
            Package: dds.mongo.mid
            <br />
            Storage space: 10 GB
            <br />
            Replication_factor:3
            <br />
            Region: East China 1<br />
          </Descriptions.Item>

          <Descriptions.Item label="price">
            {accommodation.price}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
}
