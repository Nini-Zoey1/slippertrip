import React from "react";

const Like = props => {
  let classes = "fa fa-2x fa-heart";
  if (!props.isLiked) classes += "-o";
  return (
    <i
      onClick={props.onClick}
      style={{ cursor: "pointer", paddingBottom: 80, color: "brown" }}
      className={classes}
      aria-hidden="true"
    />
  );
};

export default Like;
