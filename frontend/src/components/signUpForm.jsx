import React from "react";
import { Form, Input, Checkbox, Button } from "antd";
import { apiUrl } from "../config.json";
// const { Option } = Select;
import axios from "axios";
import QueueAnim from "rc-queue-anim";

// const apiEndpoint = apiUrl + "/user/signup/";
const endPointUrl = "http://127.0.0.1:8000/api/user/signup/";

class SignUpFormInternal extends React.Component {
  state = {
    email: "",
    password: "",
    phone: "",
    username: "",
    confirmDirty: false
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        // console.log("Received values of form: ", values);
        console.log("Received values of form-mapped: ", this.dataMap(values));
        // console.log("stringify", JSON.stringify(this.dataMap(values)));
        //call api
        axios
          .post(endPointUrl, this.dataMap(values))
          .then(res => {
            console.log(res);
          })
          .catch(error => console.log(error));
        // fetch(endPointUrl, {
        //   method: "POST",
        //   body: JSON.stringify(this.dataMap(values)),
        //   headers: {
        //     "Content-Type": "application/json"
        //   }
        // })
        //   .then(res => res.json())
        //   .then(response => console.log("Success:", JSON.stringify(response)))
        //   .catch(error => console.error("Error-message:", error));
      }
      console.log("history", this.props.history.push("/signupinfo"));
    });
    // console.log("Received values of form: ", values);
  };
  dataMap = values => {
    return {
      email: values.email,
      password: values.password,
      phone: values.phone,
      username: values.username
    };
  };
  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };
  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 18,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };

    // const prefixSelector = getFieldDecorator("prefix", {
    //   initialValue: "61"
    // })(
    //   <Select style={{ width: 70 }}>
    //     <Option value="61">+61</Option>
    //     <Option value="86">+86</Option>
    //   </Select>
    // );
    return (
      <Form
        labelAlign="left"
        layout="horizontal"
        style={{
          borderRadius: 20,
          padding: 40,
          margin: 60,
          backgroundColor: "rgb(245,247,250)"
        }}
        onSubmit={this.handleSubmit}
      >
        {/* <Row gutter={16} span={24}> */}
        <QueueAnim type="right" delay={500}>
          <Form.Item key="1" {...formItemLayout} label="E-mail">
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "The input is not valid E-mail!"
                },
                {
                  required: true,
                  message: "Please input your E-mail!"
                }
              ]
            })(<Input placeholder="Please input your E-mail" />)}
          </Form.Item>
          <Form.Item key="2" {...formItemLayout} label="Username">
            {getFieldDecorator("username", {
              rules: [
                {
                  required: true,
                  message: "Please input your  Name!"
                }
              ]
            })(<Input placeholder="Please input your  Name" />)}
          </Form.Item>
          {/* </Row> */}

          <Form.Item key="3" {...formItemLayout} label="Password" hasFeedback>
            {getFieldDecorator("password", {
              rules: [
                {
                  required: true,
                  message: "Please input your password!"
                },
                {
                  validator: this.validateToNextPassword
                }
              ]
            })(<Input.Password placeholder="Please input your password" />)}
          </Form.Item>

          <Form.Item key="4" {...formItemLayout} label="Confirm" hasFeedback>
            {getFieldDecorator("confirm", {
              rules: [
                {
                  required: true,
                  message: "Please confirm your password!"
                },
                {
                  validator: this.compareToFirstPassword
                }
              ]
            })(
              <Input.Password
                onBlur={this.handleConfirmBlur}
                placeholder="Please confirm your password"
              />
            )}
          </Form.Item>
          <Form.Item key="5" {...formItemLayout} label="Phone">
            {getFieldDecorator("phone", {
              rules: [
                { required: true, message: "Please input your phone number!" }
              ]
            })(<Input placeholder="Please input your phone number" />)}
          </Form.Item>

          <Form.Item key="6" {...tailFormItemLayout}>
            {getFieldDecorator("agreement", {
              valuePropName: "checked"
            })(
              <Checkbox>
                I have read the
                <a href="." target="_blank">
                  agreement
                </a>
              </Checkbox>
            )}
          </Form.Item>
          <Form.Item key="7" {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit" style={{ width: "50%" }}>
              Sign Up
            </Button>
          </Form.Item>
        </QueueAnim>
      </Form>
    );
  }
}
const SignUpForm = Form.create({ name: "register" })(SignUpFormInternal);

export default SignUpForm;
