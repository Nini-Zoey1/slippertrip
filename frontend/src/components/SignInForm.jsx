import React from "react";
import { Form, Icon, Input, Button, Checkbox } from "antd";
import { Link } from "react-router-dom";
// import axios from "axios";
import QueueAnim from "rc-queue-anim";

// import { apiUrl } from "../config.json";

const endpointUrl = "http://127.0.0.1:8000/api/user/login/";
class NormalLoginForm extends React.Component {
  state = {
    loading: false
  };
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        // console.log("Received values of form: ", values);
        console.log("mapped:", this.dataMap(values));
        this.setState({ loading: true });
        let fetchData = {
          method: "POST",
          body: JSON.stringify(this.dataMap(values)),
          headers: {
            "Content-Type": "application/json"
          }
        };
        fetch(endpointUrl, fetchData)
          .then(res => res.json())
          .then(res => {
            console.log("res", res);
            localStorage.setItem("token", res.data.token);
            localStorage.setItem("username", res.data.username);
            this.setState({ loading: false });

            const fromurl =
              this.props.match &&
              this.props.match.params &&
              this.props.match.params.fromurl &&
              this.props.match.params.fromurl.replace(/~/g, "/");
            // console.log(fromurl);

            // const { state } = this.props.location;
            // console.log("state.from.pathname", state.from.pathname);
            window.location = fromurl ? fromurl : "/";
            // console.log(this.props);
            // this.props.history.goBack();
            // window.location = "/";
          })
          .catch(error => console.log(error));

        // axios
        //   .post(endpointUrl, this.dataMap(values))
        //   .then(res => {
        //     // console.log("token", res.data.data.token);
        //     console.log("mapped", this.dataMap(values));
        //     console.log("res", res);
        // const token = res.data.data.token;
        // console.log(token);

        // localStorage.setItem("token", token);

        // window.location = "/";
        // })
        // .catch(error => console.log("error", error));
      }
    });
  };
  dataMap = value => {
    return { email: value.mail, password: value.password };
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <QueueAnim type="bottom">
        <Form
          key="form"
          onSubmit={this.handleSubmit}
          style={{
            maxWidth: 400,
            backgroundColor: "#A0A0A0",
            padding: 50,
            borderRadius: 20
          }}
        >
          <QueueAnim type="right" delay={500}>
            <Form.Item key="mail">
              {getFieldDecorator("mail", {
                rules: [{ required: true, message: "Please input your email!" }]
              })(
                <Input
                  size="large"
                  prefix={
                    <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Email"
                />
              )}
            </Form.Item>
            <Form.Item key="password">
              {getFieldDecorator("password", {
                rules: [
                  { required: true, message: "Please input your Password!" }
                ]
              })(
                <Input
                  size="large"
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="Password"
                />
              )}
            </Form.Item>
            <Form.Item key="remember">
              {getFieldDecorator("remember", {
                valuePropName: "checked",
                initialValue: true
              })(<Checkbox>Remember me</Checkbox>)}
              <Link style={{ float: "right" }} to="">
                Forgot password
              </Link>
              <Button
                size="large"
                type="primary"
                loading={this.state.loading}
                htmlType="submit"
                style={{ width: "100%" }}
              >
                Log in
              </Button>
              Or <Link to="/signup">register now!</Link>
            </Form.Item>
          </QueueAnim>
        </Form>
      </QueueAnim>
    );
  }
}

const SignInForm = Form.create({ name: "normal_login" })(NormalLoginForm);

export default SignInForm;
