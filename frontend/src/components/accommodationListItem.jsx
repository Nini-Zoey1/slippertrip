import React from "react";
import { Row, Col } from "antd";
import Like from "../components/common/like";

import { Link } from "react-router-dom";
import axios from "axios";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
const apiUrl = "http://127.0.0.1:8000/api/hotel/save/";

class AccommodationListItem extends React.Component {
  // item = this.props.item;
  // likedList = this.props.likedList;
  // handleLikedListAdd = this.props.handleLikedListAdd;
  // handleLikedListRemove = this.props.handleLikedListRemove;
  // isLiked = this.props.handleCheckLikedOrNot(this.item.id);
  // isLiked = this.likedList.includes(this.item.id) ? true : false;

  handleLiked = () => {
    // let isLiked = this.state.isLiked;
    // isLiked = isLiked === true ? false : true;
    // this.setState({ isLiked }, () => {
    //   if (this.state.isLiked) {
    //     //added like  api
    //     axios
    //       .post(apiUrl, fakeData)
    //       .then(res => console.log("like-res", res))
    //       .catch(error => console.log(error));
    //   } else {
    //   }
    // }
    // );
  };
  render() {
    const { item } = this.props;
    // console.log(item.id, this.isLiked);

    return (
      <Row
        style={{ margin: 20 }}
        gutter={18}
        type="flex"
        justify="space-around"
        align="top"
      >
        <Col sm={24} md={8} lg={6}>
          <img
            style={{ width: "100%", height: 170 }}
            src={item.pic_url}
            alt=""
          />
        </Col>
        <Col sm={24} md={12} lg={12}>
          <Row>
            <Col>
              <Link to={`/acclistdetails/${item.id}/${this.props.duration}/`}>
                <h5>{item.title}</h5>
                {/* <h5>{`/acclistdetails/${item.id}/${this.props.duration}/`}</h5> */}
              </Link>
            </Col>
          </Row>

          <Row>
            <Col sm={24} md={12}>
              <p>
                address: <span>{item.addr_city}</span>
              </p>
              <p>
                self_check_in: <span>{item.self_check_in ? "Yes" : "No"}</span>
              </p>
              {/* <p>
              bed_type: <span>{item.bed_type}</span>
            </p> */}
            </Col>
            <Col sm={24} md={12}>
              <p>
                rating: <span>{item.acco_score}</span>
              </p>
              <p>
                beds: <span>{item.beds}</span>
              </p>
            </Col>
          </Row>

          <Row>
            <Col>
              <p>
                amenities:
                <i
                  style={{ marginRight: 10 }}
                  className="fa fa-lg fa-wifi"
                  aria-hidden="true"
                />
                <i
                  style={{ marginRight: 10 }}
                  className="fa fa-lg fa-television"
                  aria-hidden="true"
                />
                <i
                  style={{ marginRight: 10 }}
                  className="fa fa-lg fa-shower"
                  aria-hidden="true"
                />
                <i
                  style={{ marginRight: 10 }}
                  className="fa fa-lg fa-snowflake-o"
                  aria-hidden="true"
                />
              </p>
            </Col>
          </Row>
        </Col>
        <Col sm={24} md={4} lg={4} style={{ color: "brown", fontSize: 20 }}>
          <p> price: ${item.price}</p>
        </Col>
        <Col sm={24} md={4} lg={2}>
          <Like isLiked={this.props.isLiked} onClick={this.props.handleLiked} />
        </Col>
      </Row>
    );
  }
}

export default AccommodationListItem;
