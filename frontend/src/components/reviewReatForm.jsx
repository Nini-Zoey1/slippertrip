import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import { Row, Col, Form, Icon, Input, Button, Rate } from "antd";
const { TextArea } = Input;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class innerRevireRateForm extends React.Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        axios
          .put(
            `http://127.0.0.1:8000/api/order/${
              this.props.match.params.id
            }/rate/`,
            {
              rate: 20 * values.rate,
              review: values.review
            }
          )
          .then(res => {
            console.log(res);
            this.props.history.push("/reserved");
          });
      }
    });
  };

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;

    // Only show error after a field is touched.

    return (
      <Row type="flex" justify="center">
        <Col span={8}>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item label="rate">
              {getFieldDecorator("rate", {
                rules: []
              })(<Rate />)}
            </Form.Item>
            <Form.Item label="review">
              {getFieldDecorator("review", {
                rules: []
              })(
                <TextArea placeholder="reviews.." autosize={{ minRows: 2 }} />
              )}
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                disabled={hasErrors(getFieldsError())}
              >
                Submit
              </Button>
            </Form.Item>
          </Form>{" "}
        </Col>
      </Row>
    );
  }
}

const RevireRateForm = Form.create({ name: "horizontal_login" })(
  innerRevireRateForm
);

export default RevireRateForm;
