import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";
import {
  Form,
  Input,
  Button,
  DatePicker,
  Statistic,
  InputNumber,
  Alert
} from "antd";
import axios from "axios";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
const apiUrl = "http://127.0.0.1:8000/api/order/";

class form extends React.Component {
  class = { loading: false };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        const query = {
          acco: this.props.acco,
          u_lastname: values.u_lastname,
          u_firstname: values.u_firstname,
          u_people: values.u_people,
          u_email: values.u_email,
          u_telephone: values.u_telephone,
          total_price: values.total_price,
          date_in: values["range-picker"][0].format("YYYY-MM-DD"),
          date_out: values["range-picker"][1].format("YYYY-MM-DD")
        };
        console.log("orderform-query", query);
        this.setState({ loading: true });
        axios
          .post(apiUrl, query)
          .then(res => {
            console.log(res);
            // return to account
            this.setState({ loading: false });

            this.props.history.push("/reserved");
          })
          .catch(error => console.log(error));
      }
    });
  };

  handleSelectChange = value => {
    console.log(value);
    // this.props.form.setFieldsValue({
    //   note: `Hi, ${value === 'male' ? 'man' : 'lady'}!`,
    // });
  };

  handleTimePrice = () => {
    const startDate = moment(
      this.props.form.getFieldValue("range-picker")[0].format("YYYY-MM-DD")
    );
    const endDate = moment(
      this.props.form.getFieldValue("range-picker")[1].format("YYYY-MM-DD")
    );
    const price = parseFloat(this.props.price);
    const duration = endDate.diff(startDate, "days");
    this.props.form.setFieldsValue({
      total_price: duration * price
    });
  };
  handleDateAvailable = () => {
    const url = `http://127.0.0.1:8000/api/hotel/${
      this.props.acco
    }/check/?date_in=${this.props.form
      .getFieldValue("range-picker")[0]
      .format("YYYY-MM-DD")}&date_out=${this.props.form
      .getFieldValue("range-picker")[1]
      .format("YYYY-MM-DD")}
   `;
    console.log("checkdateurl", url);
    axios.get(url).then(res => {
      console.log("datecheck-res", res);
      this.props.handleDateMessage(res.data.message);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { RangePicker } = DatePicker;

    return (
      <Form onSubmit={this.handleSubmit}>
        <span onClick={this.handleDateAvailable}>test</span>
        <Form.Item label="lastname">
          {getFieldDecorator("u_lastname", {
            rules: [{ required: true, message: "Please input your lastname!" }]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="firstname">
          {getFieldDecorator("u_firstname", {
            rules: [{ required: true, message: "Please input your firstname!" }]
          })(<Input />)}
        </Form.Item>

        <Form.Item key="d" label="check in  & check out date">
          {getFieldDecorator("range-picker", {
            rules: [
              {
                type: "array",
                required: true,
                message: "Please select time!"
              }
            ]
          })(<RangePicker disabledDate={this.disabledDate} />)}
        </Form.Item>
        <Form.Item label="guests">
          {getFieldDecorator("u_people", {
            initialValue: 1,
            rules: [{ required: true, message: "Please input no. of people!" }]
          })(<InputNumber onChange={this.handleTimePrice} min={1} />)}
        </Form.Item>
        <Form.Item label="telephone">
          {getFieldDecorator("u_telephone", {
            rules: [{ required: true, message: "Please input your telephone!" }]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="email">
          {getFieldDecorator("u_email", {
            rules: [
              {
                type: "email",
                required: true,
                message: "Please input your u_email!"
              }
            ]
          })(<Input />)}
        </Form.Item>

        <Form.Item label="">
          {getFieldDecorator("total_price", {
            rules: []
          })(
            <Statistic title="total price" precision={2} />

            // <Input disabled size="large"  />
          )}
          {/* <span className="ant-form-text">China</span> */}
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
          <Button type="primary" htmlType="submit" size="large">
            Confirm Order
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const OrderForm = Form.create({ name: "OrderForm" })(form);

export default OrderForm;
