import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import { Row, Col, Form, Input, Icon, Button, Checkbox } from "antd";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
const formItemLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 }
};
const formTailLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 8, offset: 8 }
};
class innerform extends React.Component {
  state = {
    checkNick: false
  };

  check = () => {
    this.props.form.validateFields(err => {
      if (!err) {
        console.info("success");
      }
    });
  };

  handleFormSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        axios
          .put("http://127.0.0.1:8000/api/user/", {
            user: {
              username: values.username,
              phone: values.phone,
              password: values.password
            }
          })
          .then(res => {
            console.log(res);
            this.props.history.push("/userprofile");
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { user } = this.props;
    return (
      <div >
        <Row type="flex" justify="center" style={{ padding: 20 }}>
          <Col span={20}>
            <Form {...formItemLayout}>
              <Form.Item {...formItemLayout} label="Name">
                {getFieldDecorator("username", {
                  initialValue: user.username,
                  rules: []
                })(<Input placeholder="" />)}
              </Form.Item>
              <Form.Item {...formItemLayout} label="phone">
                {getFieldDecorator("phone", {
                  initialValue: user.phone,

                  rules: []
                })(<Input placeholder="" />)}
              </Form.Item>
              <Form.Item key="password" label="password">
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "Please input your Password!" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    placeholder="Password"
                  />
                )}
              </Form.Item>

              <Form.Item {...formTailLayout}>
                <Button type="primary" onClick={this.handleFormSubmit} style={{ marginLeft: "400px" }}>
                  Confirm
              </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

const EditUserProfileForm = Form.create({ name: "dynamic_rule" })(innerform);
export default EditUserProfileForm;
