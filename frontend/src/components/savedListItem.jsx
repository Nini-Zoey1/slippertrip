import React, { Component } from "react";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";

class SavedListItem extends Component {
  render() {
    const { item } = this.props;
    console.log("item", item);
    return (
      <Row
        style={{
          margin: 20,
          border: "2px solid rgba(129, 71, 14,0.5)",
          padding: "25px 10px 0 5px",
          borderRadius: "10px",
          height: "230px"
        }}
        gutter={18}
        type="flex"
        justify="space-around"
        align="top"
      >
        <Col sm={24} md={8} lg={6}>
          <img
            style={{ width: "100%", height: 170, borderRadius: "5px" }}
            src={item.pic_url}
            alt=""
          />
        </Col>
        <Col sm={24} md={12} lg={12}>
          <Row>
            <Col>
              <Link to={`/acclistdetails/${item.id}/${this.props.duration}/`}>
                <p style={{ fontSize: "17px", color: "rgb(55,33,10)" }}>
                  <strong>{item.title}</strong>
                </p>
                {/* <h5>{`/acclistdetails/${item.id}/${this.props.duration}/`}</h5> */}
              </Link>
            </Col>
          </Row>

          <Row>
            <p style={{ fontSize: "15px" }}>
              <span style={{ color: "rgb(104,66,9)", fontSize: "15px" }}>
                Address:
              </span>{" "}
              <span>{item.addr_city}</span>{" "}
            </p>

            <p style={{ fontSize: "15px" }}>
              <span style={{ color: "rgb(104,66,9)", fontSize: "15px" }}>
                {" "}
                Create_time:{" "}
              </span>
              <span>{item.create_time.slice(0, 10)}</span> <br />
            </p>
            {/* <p>
              bed_type: <span>{item.bed_type}</span>
            </p> */}

            {/* <Col sm={24} md={24}>
              <p>
                reviews_number: <span>{item.reviews_number}</span>
              </p>
            </Col> */}
          </Row>

          <Row>
            <Col sm={24} md={24} style={{ color: "brown", fontSize: 17 }}>
              <p>
                {" "}
                <strong> Price: ${item.price} </strong>
              </p>
            </Col>
          </Row>
        </Col>

        <Col sm={6} md={4} lg={2}>
          <Button onClick={() => this.props.onDelete(item.id)} type="primary">
            Delete
          </Button>
        </Col>
      </Row>
    );
  }
}

export default SavedListItem;
