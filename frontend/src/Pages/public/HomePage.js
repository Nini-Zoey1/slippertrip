import React from "react";
//import NaviBar from "./NaviBar";
import Paper from "@material-ui/core/Paper";
import "./homepage.css";
import Grid from "@material-ui/core/Grid";
// import QueueAnim from "rc-queue-anim";
// import MenuItem from "@material-ui/core/MenuItem";
// import FormControl from "@material-ui/core/FormControl";
// import Select from "@material-ui/core/Select";
// import { Form, Col, Row } from "react-bootstrap";
import HomepageForm from "../../components/homepageForm";

class Home extends React.Component {
  state = {
    addr_city: "Mascot",
    start_date: "2019-10-12",
    end_date: "2019-10-13",
    people: 2
  };

  // validate form input correctly

  handleChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  //prevent event execute automatically when page renders
  handleSubmit = values => {
    const queryString = `?addr_city=${values.addr_city}&people=${
      values.people
    }&start_date=${values.start_date}&end_date=${values.end_date} `;
    console.log("homepage-queryString", queryString);
    // console.log(this.props);
    this.props.history.push(`/acclist/${queryString}`);
  };

  render() {
    return (
      <div style={{ width: "100%" }} className="image">
        <Grid container justify="flex-start">
          <Paper
            key="paper"
            style={{ backgroundColor: "rgba(255, 255, 255, 0.98)" }}
            className="search"
          >
            <h4 className="search-title">Book unique places to stay </h4>
            <HomepageForm onSubmit={this.handleSubmit} />
            {/* <Form onSubmit={this.handleSubmit}>
              <QueueAnim type="bottom" delay={600}>
                <Row key="where">
                  <Col>
                    <Form.Group controlId="where">
                      <Form.Label className="FormLabel">Destination</Form.Label>
                      <Form.Control
                        className="Input"
                        type="text"
                        placeholder="Anyplace"
                        value={this.state.addr_city}
                        onChange={this.handleChange}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row key="datehomepage">
                  <Col>
                    <Form.Group controlId="date">
                      <Form.Label className="FormLabel">Check-In</Form.Label>
                      <Form.Control
                        className="Input"
                        type="date"
                        value={this.state.start_date}
                        onChange={this.handleChange}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row key="date">
                  <Col>
                    <Form.Group controlId="date">
                      <Form.Label className="FormLabel">Check-Out</Form.Label>
                      <Form.Control
                        className="Input"
                        type="date"
                        value={this.state.end_date}
                        onChange={this.handleChange}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row key="guest">
                  <Col>
                    <Form.Group controlId="guest">
                      <Form.Label className="FormLabel-1">Guests</Form.Label>
                      <FormControl>
                        <Select
                          className="select"
                          value={this.state.people}
                          onChange={this.handleChange}
                        >
                          <MenuItem value={"1"}>1</MenuItem>
                          <MenuItem value={"2"}>2</MenuItem>
                          <MenuItem value={"3"}>3</MenuItem>
                          <MenuItem value={"4"}>4</MenuItem>
                          <MenuItem value={">4"}>>4</MenuItem>
                        </Select>
                      </FormControl>
                    </Form.Group>
                  </Col>
                </Row>
                <Row key="Button">
                  <Col>
                    <button className="Button" type="submit">
                      Search
                    </button>
                  </Col>
                </Row>
              </QueueAnim>
            </Form> */}
          </Paper>
        </Grid>
      </div>
    );
  }
}
export default Home;
