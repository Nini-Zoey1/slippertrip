import React from "react";
//import NaviBar from "./NaviBar";
import { Row, Col } from "antd";
import SignInForm from "../../components/SignInForm";

class Login extends React.Component {
  render() {
    return (
      <div
        style={{
          background:
            "linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url('https://a0.muscache.com/im/pictures/61593199/93cd2abc_original.jpg?aki_policy=xx_large')",
          // backgroundImage:
          //   "url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          backgroundSize: "cover"
        }}
      >
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ height: 700 }}
        >
          <Col>
            <SignInForm
              location={this.props.location}
              history={this.props.history}
              match={this.props.match}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Login;
