import React from "react";
//import NaviBar from "./NaviBar";
import SignUpForm from "../../components/signUpForm";
import { Row, Col } from "antd";

class Signup extends React.Component {
  render() {
    return (
      <div
        style={{
          background:
            "linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          // backgroundImage:
          //   "url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          backgroundSize: "cover"
        }}
      >
        <Row type="flex" justify="center">
          <Col sm={24} md={10} style={{ borderRadius: 20 }}>
            <SignUpForm history={this.props.history} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Signup;
