import React from "react";
// import { Result, Button } from "antd";

const NotFound = () => (
  <div>
    <h2>404</h2>
    <p>Sorry, the page you visited does not exist</p>
  </div>
  // <Result
  //   status="404"
  //   title="404"
  //   subTitle="Sorry, the page you visited does not exist."
  //   extra={<Button type="primary">Back Home</Button>}
  // />
);

export default NotFound;
