import React, { Component } from "react";
import { Descriptions, Divider } from "antd";
import Orderaccommodationdetails from "../accommodation/orderpartaccommodationdetails";
import axios from "axios";
class OrderDetailers extends Component {
  state = {
    data: {
      u_user: {
        u_user_id: 3
      },
      order_info: {
        id: 16,
        create_time: "2019-07-26T22:36:44.942",
        update_time: "2019-07-26T22:36:44.943",
        is_delete: false,
        acco: 5,
        u_user: 3,
        u_lastname: "li",
        u_firstname: "xu",
        u_people: 2,
        u_telephone: "1234567890",
        u_email: "xulizoey@163.com",
        date_in: "2019-10-18",
        date_out: "2019-10-19",
        total_price: 999,
        review: null,
        rate: null
      },
      host_user: {
        username: "xl",
        email: "xulizoey@163.com",
        phone: "1234567890"
      },
      acco_info: {
        id: 5,
        create_time: "2019-07-17T22:42:31.460",
        update_time: "2019-07-26T22:36:44.069",
        is_delete: false,
        host_user_id: 2,
        title: "aaaaa",
        addr_street: "2 Muller Lane",
        addr_city: "Mascot",
        people: 4,
        start_date: "2019-09-11",
        end_date: "2019-11-30",
        reserved_date:
          "2019-10-14,2019-10-15|2019-10-15,2019-10-16|2019-10-17,2019-10-17|2019-10-18,2019-10-19",
        price: 400,
        acco_score: "5.0",
        reviews_number: 0,
        beds: 2,
        bedrooms: 2,
        carparks: 1,
        latitude: "-33.9218200",
        longtitude: "151.1853700",
        description: "dddddddd",
        air_conditioner: true,
        tv: true,
        wifi: true,
        telephone: true,
        shampoo: true,
        self_check_in: true,
        hair_dryer: true,
        freezer: true,
        pic1:
          "https://a0.muscache.com/im/pictures/763ad5c8-c951-43e0-b926-4a98c25c45e8.jpg?aki_policy=large",
        pic2: "",
        pic3: "",
        pic4: "",
        pic5: "",
        pic6: ""
      }
    }
  };
  componentDidMount() {
    axios
      .get(
        `http://127.0.0.1:8000/api/order/${this.props.match.params.id}/view/`
      )
      .then(res => {
        console.log("detailed", res);
        this.setState({ data: res.data.data });
      });
  }
  render() {
    return (
      <div style={{ width: 900, margin: " 20px auto" }}>
        <Descriptions title="order info" layout="vertical" bordered column={2}>
          {/* <Descriptions.Item label="id">
            {this.state.data.order_info.id}
          </Descriptions.Item> */}
          <Descriptions.Item label="create_time">
            {this.state.data.order_info.create_time}
          </Descriptions.Item>
          <Descriptions.Item label="update_time">
            {this.state.data.order_info.update_time}
          </Descriptions.Item>
          <Descriptions.Item label="date_in">
            {this.state.data.order_info.date_in}
          </Descriptions.Item>
          <Descriptions.Item label="date_out">
            {this.state.data.order_info.date_out}
          </Descriptions.Item>
        </Descriptions>
        <Divider />

        <Orderaccommodationdetails data={this.state.data} />
      </div>
    );
  }
}

export default OrderDetailers;
