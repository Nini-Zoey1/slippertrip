import React from "react";
import "./account.css";
import { Link } from "react-router-dom";

class Account extends React.Component {
  render() {
    return (
      <div>
        <div style={{ width: "100%" }} className="image2" />
        <div >
          <p style={{ marginLeft: "20px" }}>HOME/<span style={{ color: "rgb(104, 70, 19)" }}>ACCOUNT</span></p>
          <h2 className="title1">Slippertrip Accommodation</h2>
          <p style={{ margin: "20px  200px", textAlign: "center" }} >In this module you can post your Houses and Accommodation which help you to provide the wonderful living experience for the other person. Apart from that, management your account and your history details. </p>
        </div>
        <div className="row">
          <div className="image3">
            <p className="link3">
              <Link to="/property" className="tags"><strong>My Property</strong></Link>
            </p>
          </div>
          <div className="image4">
            <div className="row">
              <div className="col-sm image4-1">
                <p className="link-1">
                  <Link to="/saved"><strong>Saved</strong></Link>
                </p>
              </div>
            </div>
            <div className="row">
              <div className="image4-2">
                <p className="link-1">
                  <Link to="/reserved"><strong>Reserved</strong></Link>
                </p>
              </div>
            </div>
          </div>
          <div className="image5">
            <p className="link4">
              <Link to="/userprofile"><strong>Userfile</strong></Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default Account;
