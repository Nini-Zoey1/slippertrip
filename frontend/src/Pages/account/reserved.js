import React from "react";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./reserved.css";
import axios from "axios";
import { apiUrl } from "../../config.json";
import { List, Button, Spin } from "antd";
import ReservedListItem from "../../components/reservedListItem";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));

const endpointUrl = apiUrl + "/order";
class reserved extends React.Component {
  state = {
    // acco_id_list: [],
    order_info: [],

    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(endpointUrl)
      .then(res => {
        console.log("reserved res", res);
        this.setState({
          order_info: res.data.data.order_info,
          loading: false
        });
      })
      .catch(error => console.log(error));
  }
  handleDelete = id => {
    this.setState({ loading: true });
    console.log("delete-url", `http://127.0.0.1:8000/api/order/${id}/delete/`);

    const original_order_info = this.state.order_info;
    axios
      .delete(`http://127.0.0.1:8000/api/order/${id}/delete/`)
      .then(res => {
        console.log("deletd-res", res);
        const order_info = original_order_info.filter(
          item => item.order_detail.id !== id
        );
        this.setState({ order_info });
        this.setState({ loading: false });
      })
      .catch(error => {
        console.log(error);
        this.setState({ order_info: original_order_info });
      });
  };
  handleReview = id => {
    this.props.history.push(`/reviewrate/${id}`);
  };

  render() {
    return (
      <div style={{ width: "100%" }} className="imageres">
        <Container bsPrefix="containersytle" style={{ width: "100 %" }}>
          <Row style={{ minHeight: "700px" }}>
            <Col
              xs={12}
              md={{ span: 8, offset: 2 }}
              style={{
                backgroundColor: "rgba(255, 255, 255, 1)",
                marginTop: "120px",
                marginBottom: "200px",
                borderRadius: "20px"
              }}
            >
              <p
                style={{
                  marginTop: "30px"
                }}
              >
                <Link to="/property">
                  <span className="tag3">My Property</span>
                </Link>

                <Link to="/saved">
                  <span className="tag3">Saved</span>
                </Link>

                <Link to="/reserved">
                  <span className="tag3">Reserved</span>
                </Link>

                <Link to="/userprofile">
                  <span className="tag3">UserProfile</span>
                </Link>
              </p>
              <List
                style={{ margin: 25, padding: 20 }}
                header={<h4>My Reserved Places</h4>}
                // footer={<div>Footer</div>}
                bordered
                loading={this.state.loading && <Spin />}
                dataSource={this.state.order_info}
                renderItem={item => (
                  <List.Item key={item.id} style={{}}>
                    <ReservedListItem
                      onReview={this.handleReview}
                      item={item}
                      onDelete={this.handleDelete}
                    />
                    {/* <div>
                      <Col>{`title:  ${item.title}`}</Col>
                    </div> */}
                    {/* <div>
                      <Col>{`addr_city:  ${item.addr_city}`}</Col>
                    </div>
                    <div>
                      <Col>{`score rating:  ${item.acco_score}`}</Col>
                    </div>
                    <div>
                      <Col>{`price:  ${item.price}`}</Col>
                    </div> */}
                    {/* <div>
                      <Col>
                        <Button
                          onClick={() => this.handleDelete(item.id)}
                          loading={this.loading}
                        >
                          Delete
                        </Button>
                      </Col>
                    </div> */}
                  </List.Item>
                )}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default reserved;
