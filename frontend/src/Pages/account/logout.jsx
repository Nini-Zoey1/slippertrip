import React, { Component } from "react";
class Logout extends Component {
  componentDidMount() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");

    window.location = "/";
  }
  render() {
    return null;
  }
}

export default Logout;
