import React, { Component } from "react";
import { Row, Col, Alert } from "antd";
import SignInForm from "../../components/SignInForm";
class ValidationSuccess extends Component {
  state = {};
  render() {
    return (
      <div
        style={{
          background:
            "linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          // backgroundImage:
          //   "url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          backgroundSize: "cover",
          padding: 30
        }}
      >
        <Row type="flex" justify="center" style={{ padding: 30 }}>
          <Col style={{}}>
            <Alert message="email validated! Please login" type="success" />
          </Col>
        </Row>
        <Row type="flex" justify="center">
          <SignInForm />
        </Row>
      </div>
    );
  }
}

export default ValidationSuccess;
