import React from "react";
import "./edituserprofile.css";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Descriptions, Spin, Button } from "antd";
import axios from "axios";
import { apiUrl } from "../../config.json";
import EditUserProfileForm from "../../components/editUserProfileForm";

localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
const endpointUrl = apiUrl + "/user/";

class EditUserprofile extends React.Component {
  state = { user: {}, loading: false };
  componentDidMount() {
    this.setState({ loading: true });

    axios.get(endpointUrl).then(res => {
      console.log(res.data.data);
      let user = {
        id: res.data.data.id,
        date_joined: res.data.data.date_joined,
        email: res.data.data.email,
        last_login: res.data.data.last_login,
        phone: res.data.data.phone,
        username: res.data.data.username
      };
      this.setState({ user, loading: false });
    });
  }
  handleEdit = id => {
    console.log(id);
  };
  render() {
    return (
      <div style={{ width: "100%" }} className="imageuser">
        <Container bsPrefix="containersytle" style={{ width: "100 %" }}>
          <Row style={{ minHeight: "750px" }}>
            <Col
              xs={12}
              md={{ span: 8, offset: 2 }}
              style={{
                backgroundColor: "rgba(255, 255, 255, 1)",
                marginTop: "120px",
                marginBottom: "200px",
                borderRadius: "20px"
              }}
            >
              <p style={{
                marginTop: "40px"
              }}>
                <Link to="/property">
                  <span className="tag4">My Property</span>
                </Link>
                <Link to="/saved">
                  <span className="tag4">Saved</span>
                </Link>
                <Link to="/reserved">
                  <span className="tag4">Reserved</span>
                </Link>
                <Link to="/userprofile">
                  <span className="tag4">UserProfile</span>
                </Link>
              </p>
              {this.state.loading ? (
                <div
                  style={{
                    width: 100,
                    height: 200,
                    marginLeft: "auto",
                    marginRight: "auto",
                    marginTop: 60
                  }}
                >
                  <Spin />
                </div>
              ) : (
                  <div>
                    <EditUserProfileForm
                      history={this.props.history}
                      user={this.state.user}
                    />
                  </div>
                )}
            </Col>
          </Row>
          <Row />
        </Container>
      </div>
    );
  }
}

export default EditUserprofile;
