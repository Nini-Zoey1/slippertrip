import React from "react";
import "./myproperty.css";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Button, List, Spin, Skeleton } from "antd";
//import NaviBar from "./NaviBar";
import axios from "axios";
import AccountListItem from "../../components/accountListItem";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));

const apiUrl = "http://127.0.0.1:8000/api/hotel/";

class myproperty extends React.Component {
  state = {
    // acco_id_list: [],
    acco_info: [],
    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(apiUrl)
      .then(res => {
        console.log("property res", res);
        const acco_info = [...res.data.data.acco_info];
        this.setState({ acco_info, loading: false });
      })
      .catch(error => console.log(error));
  }
  handleDelete = id => {
    this.setState({ loading: true });

    const original_acco_info = this.state.acco_info;

    axios
      .delete(`http://127.0.0.1:8000/api/hotel/${id}/delete/`)
      .then(res => {
        console.log("deletd-res", res);
        const acco_info = original_acco_info.filter(item => item.id !== id);
        this.setState({ acco_info });
        this.setState({ loading: false });
      })
      .catch(error => {
        console.log(error);
        this.setState({ acco_info: original_acco_info });
      });
  };
  handleEdit = id => {
    // console.log(id);
    this.props.history.push(`/editpostform/${id}`);
  };
  render() {
    return (
      <div style={{ width: "100%" }} className="imagepro">
        <Container bsPrefix="containersytle" style={{ width: "100 %" }}>
          <Row style={{ minHeight: "600px" }}>
            <Col
              xs={12}
              md={{ span: 8, offset: 2 }}
              style={{
                backgroundColor: "rgba(255, 255, 255, 0.95)",
                marginTop: "120px",
                marginBottom: "200px",
                borderRadius: "20px"
              }}
            >
              <p
                style={{
                  marginTop: "30px"
                }}
              >
                <Link to="/property">
                  <span className="tag1">My Property</span>
                </Link>
                <Link to="/saved">
                  <span className="tag1">Saved</span>
                </Link>
                <Link to="/reserved">
                  <span className="tag1">Reserved</span>
                </Link>
                <Link to="/userprofile">
                  <span className="tag1">UserProfile</span>
                </Link>
              </p>
              <p className="listplace">
                <Link to="/postform">
                  <span style={{ color: "rgba(255, 255, 255, 1)" }}>
                    Post Your Accommodation
                  </span>
                </Link>
              </p>

              <List
                pagination={{
                  onChange: page => {
                    // console.log(page);
                  },
                  pageSize: 5
                }}
                style={{ minHeight: 400 }}
                header={<h4>My Listed Places</h4>}
                loading={this.state.loading && <Spin />}
                // footer={<div>Footer</div>}
                bordered
                dataSource={this.state.acco_info}
                itemLayout="horizontal"
                renderItem={item => (
                  <AccountListItem
                    item={item}
                    onDelete={this.handleDelete}
                    onEdit={this.handleEdit}
                  />
                  // <List.Item
                  //   actions={[
                  //     <Button onClick={() => this.handleEdit(item)}>
                  //       edit
                  //     </Button>,
                  //     <span>qw</span>
                  //   ]}
                  // >
                  //   {/* <Skeleton title={false} loading={this.state.loading} active> */}
                  //   <List.Item.Meta
                  //     // avatar={
                  //     //   <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                  //     // }
                  //     title={<a href="https://ant.design">{item.title}</a>}
                  //     // description={`create_time is :${
                  //     //   item.create_time
                  //     // },the price is :${item.price}`}
                  //     description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                  //   />
                  //   {/* </Skeleton> */}
                  // </List.Item>
                  // <List.Item key={item.id} style={{}}>
                  //   <div>
                  //     <Col>
                  //       <Link to={`/acclistdetails/${item.id}/`}>{`title:  ${
                  //         item.title
                  //       }`}</Link>
                  //     </Col>
                  //   </div>
                  //   <div>
                  //     <Col>{`create_time:  ${item.create_time}`}</Col>
                  //   </div>
                  //   <div>
                  //     <Col>{`description:  ${item.description}`}</Col>
                  //   </div>
                  //   <div>
                  //     <Col>{`price:  ${item.price}`}</Col>
                  //   </div>
                  //   <div>
                  //     <Col>
                  //       <Button
                  //         onClick={() => this.handleDelete(item.id)}
                  //         loading={this.loading}
                  //       >
                  //         Delete
                  //       </Button>
                  //     </Col>
                  //   </div>
                  // </List.Item>
                  // <List.Item
                  //   key={item.title}
                  //   // actions={[
                  //   //   <IconText type="star-o" text="156" />,
                  //   //   <IconText type="like-o" text="156" />,
                  //   //   <IconText type="message" text="2" />
                  //   // ]}
                  //   // extra={<img width={100} alt="logo" src={item.pic_url} />}
                  // >
                  //   <List.Item.Meta
                  //   // avatar={<Avatar src={item.avatar} />}
                  //   // title={<Link to="#"></Link>}
                  //   // description={`create_time: ${item.create_time}`}price
                  //   />

                  // </List.Item>
                )}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default myproperty;
