import React from "react";
import "./userprofile.css";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Descriptions, Spin, Button } from "antd";
import axios from "axios";
import { apiUrl } from "../../config.json";

localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
const endpointUrl = apiUrl + "/user/";

class Userprofile extends React.Component {
  state = { user: {}, loading: false };
  componentDidMount() {
    this.setState({ loading: true });

    axios.get(endpointUrl).then(res => {
      console.log(res.data.data);
      let user = {
        id: res.data.data.id,
        date_joined: res.data.data.date_joined.slice(0, 10),
        email: res.data.data.email,
        last_login: res.data.data.last_login.slice(0, 10),
        phone: res.data.data.phone,
        username: res.data.data.username
      };
      this.setState({ user, loading: false });
    });
  }
  handleEdit = id => {
    console.log(id);
    this.props.history.push(`/edituserprofile/${id}`);
  };
  render() {
    return (
      <div style={{ width: "100%" }} className="imageuser">
        <Container bsPrefix="containersytle" style={{ width: "100 %" }}>
          <Row style={{ minHeight: "750px" }}>
            <Col
              xs={12}
              md={{ span: 8, offset: 2 }}
              style={{
                backgroundColor: "rgba(255, 255, 255, 1)",
                marginTop: "120px",
                marginBottom: "200px",
                borderRadius: "20px"
              }}
            >
              <p style={{
                marginTop: "30px"
              }}>
                <Link to="/property">
                  <span className="tag4">My Property</span>
                </Link>
                <Link to="/saved">
                  <span className="tag4">Saved</span>
                </Link>
                <Link to="/reserved">
                  <span className="tag4">Reserved</span>
                </Link>
                <Link to="/userprofile">
                  <span className="tag4">UserProfile</span>
                </Link>
              </p>
              {this.state.loading ? (
                <div
                  style={{
                    width: 100,
                    height: 200,
                    marginLeft: "auto",
                    marginRight: "auto",
                    marginTop: 40
                  }}
                >
                  <Spin />
                </div>
              ) : (
                  <div>
                    <Descriptions
                      size="middle"
                      title="Account Info"
                      bordered
                      border
                      column={1}
                    >
                      <Descriptions.Item label="Date_joined" style={{ color: "rgb(156, 105, 28)" }}>
                        {this.state.user.date_joined}
                      </Descriptions.Item>
                      <Descriptions.Item label="E-mail">
                        {this.state.user.email}
                      </Descriptions.Item>
                      <Descriptions.Item label="Username">
                        {this.state.user.username}
                      </Descriptions.Item>
                      <Descriptions.Item label="Last_login">
                        {this.state.user.last_login}
                      </Descriptions.Item>
                      <Descriptions.Item label="Phone">
                        {this.state.user.phone}
                      </Descriptions.Item>
                    </Descriptions>
                    <Row style={{ padding: 10 }}>
                      <Col style={{ marginLeft: "85%" }} >
                        <Button
                          type="primary"
                          onClick={() => this.handleEdit(this.state.user.id)}
                        >
                          Edit
                      </Button>
                      </Col>{" "}
                    </Row>
                  </div>
                )}
            </Col>
          </Row>
          <Row />
        </Container>
      </div>
    );
  }
}

export default Userprofile;
