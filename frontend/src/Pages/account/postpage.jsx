import React, { Component } from "react";
import PostForm from "../../components/postForm";
class PostPage extends Component {
  state = {};
  render() {
    return (
      // <div
      //   style={{
      //     maxWidth: 900,
      //     marginTop: 30,

      //     marginLeft: "auto",
      //     marginRight: "auto"
      //   }}
      // >
      <div
        style={{
          background:
            "linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url('https://a0.muscache.com/im/pictures/61593083/365c74d1_original.jpg?aki_policy=xx_large')",
          backgroundSize: "cover",
          marginTop: "-60px",
          padding: 40
        }}
      >
        <div style={{ marginTop: "60px" }}>
          <PostForm history={this.props.history} />
        </div>

      </div>
    );
  }
}

export default PostPage;
