import React from "react";
import "./saved.css";
//import NaviBar from "./NaviBar";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import { apiUrl } from "../../config.json";
import { List, Button, Spin } from "antd";
import SavedListItem from "../../components/savedListItem";

localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));

const endpointUrl = apiUrl + "/hotel/save/";
class saved extends React.Component {
  state = {
    // acco_id_list: [],
    saved_acco_list: [],
    loading: false
  };
  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(endpointUrl)
      .then(res => {
        console.log("saved_list res", res);
        const saved_acco_list = [...res.data.data.saved_acco_list];
        this.setState({ saved_acco_list, loading: false });
      })
      .catch(error => console.log(error));
  }
  handleDelete = id => {
    this.setState({ loading: true });

    const original_list = this.state.saved_acco_list;

    axios
      .delete(`http://127.0.0.1:8000/api/hotel/${id}/unsave/`)
      .then(res => {
        console.log("deleted-res", res);
        const saved_acco_list = original_list.filter(item => item[0].id !== id);

        this.setState({ saved_acco_list });
        this.setState({ loading: false });
      })
      .catch(error => {
        console.log(error);
        // this.setState({ saved_acco_list: original_list });
      });
  };
  render() {
    return (
      <div style={{ width: "100%" }} className="imagesave">
        <Container bsPrefix="containersytle" style={{ width: "100 %" }}>
          <Row style={{ minHeight: "700px" }}>
            <Col
              xs={12}
              md={{ span: 8, offset: 2 }}
              style={{
                backgroundColor: "rgba(255, 255, 255, 1)",
                marginTop: "120px",
                marginBottom: "200px",
                borderRadius: "20px"
              }}
            >
              <p style={{
                marginTop: "30px"
              }}>
                <Link to="/property">
                  <span className="tag2">My Property</span>
                </Link>
                <Link to="/saved">
                  <span className="tag2">Saved</span>
                </Link>
                <Link to="/reserved">
                  <span className="tag2">Reserved</span>
                </Link>
                <Link to="/userprofile">
                  <span className="tag2">UserProfile</span>
                </Link>
              </p>
              <List
                style={{ margin: 25, padding: 20 }}
                header={<h4>My Saved Places</h4>}
                // footer={<div>Footer</div>}
                bordered
                loading={this.state.loading && <Spin />}
                dataSource={this.state.saved_acco_list}
                renderItem={item => (
                  <List.Item key={item[0].id} style={{}}>
                    <SavedListItem
                      item={item[0]}
                      onDelete={this.handleDelete}
                    />
                  </List.Item>
                )}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default saved;
