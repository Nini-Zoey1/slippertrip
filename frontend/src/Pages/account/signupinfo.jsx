import React, { Component } from "react";
import { Row, Col } from "antd";
import "./signupinfo.css";

class SignupInfo extends Component {
  render() {
    return (
      <div className="background">
        <Row
          style={{ minHeight: 500 }}
          type="flex"
          justify="center"
          align="middle"
        >
          <div className="container">
            <div style={{}}>
              <Row
                style={{ height: 100 }}
                type="flex"
                justify="center"
                align="middle"
              >
                <i className="fa fa-paper-plane-o fa-5x" aria-hidden="true" />
              </Row>
            </div>
            <Row
              style={{ height: 100 }}
              type="flex"
              justify="center"
              align="middle"
            >
              <Col>
                <h4>Verify your email address</h4>
              </Col>
            </Row>
            <Row
              style={{ height: 100 }}
              type="flex"
              justify="center"
              align="middle"
            >
              <Col>
                <p>
                  A verification link has been sent to your email account.
                  Please click on the link that has just been sent to your email
                  account to varify your email account continue the registration
                  process.
                </p>
              </Col>
            </Row>
          </div>
        </Row>
      </div>
    );
  }
}

export default SignupInfo;
