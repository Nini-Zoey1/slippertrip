import React from "react";
import { Carousel, Descriptions, Row, Col, Divider } from "antd";
import QueueAnim from "rc-queue-anim";

//import NaviBar from "./NaviBar";
class Orderaccommodationdetails extends React.Component {
  state = {};

  render() {
    const host_user = this.props.data.host_user;
    const acco_info = this.props.data.acco_info;
    return (
      // <div
      //   style={{
      //     maxWidth: 900,
      //     backgroundColor: "#eee",
      //     border: 2,
      //     marginleft: "auto",
      //     marginRight: "auto"
      //   }}
      // ></div>

      <div>
        <div>
          <QueueAnim delay={600}>
            <Row key="o1" gutter={40}>
              <Col sm={24} md={14}>
                <Carousel>
                  <div>
                    <img
                      style={{ width: "100%", borderRadius: 10 }}
                      src={acco_info.pic_url}
                      alt=""
                    />
                  </div>
                  <div>
                    <img
                      style={{ width: "100%", borderRadius: 10 }}
                      src="https://a0.muscache.com/im/pictures/3f8ec93f-6d07-4d5c-8635-edfc3adb7e77.jpg?aki_policy=xx_large"
                      alt=""
                    />
                  </div>
                  <div>
                    <img
                      style={{ width: "100%", borderRadius: 10 }}
                      src="https://a0.muscache.com/im/pictures/79fff1ed-262e-4be6-b0a9-8842cdcefc83.jpg?aki_policy=xx_large"
                      alt=""
                    />
                  </div>
                </Carousel>
              </Col>
              <Col sm={24} md={10}>
                <QueueAnim delay={500} type="bottom">
                  <Row key="r1">
                    <Descriptions title="host Info" layout="vertical" bordered>
                      <Descriptions.Item label="email" span={3}>
                        {host_user.email}
                      </Descriptions.Item>
                      <Descriptions.Item label="username" span={3}>
                        {host_user.username}
                      </Descriptions.Item>
                      <Descriptions.Item label="phone" span={3}>
                        {host_user.phone}
                      </Descriptions.Item>
                    </Descriptions>
                  </Row>
                </QueueAnim>
              </Col>
            </Row>
            <Divider style={{ height: 2, color: "gray" }} />

            <Row key="o2">
              <Descriptions title="Accommodation Info" layout="" bordered>
                <Descriptions.Item label="title" span={3}>
                  {acco_info.title}
                </Descriptions.Item>
                <Descriptions.Item label="addr_street" span={2}>
                  {acco_info.addr_street}
                </Descriptions.Item>
                <Descriptions.Item label="addr_city">
                  {acco_info.addr_city}
                </Descriptions.Item>
                <Descriptions.Item label="capacity/ No. of people">
                  {acco_info.people}
                </Descriptions.Item>
                <Descriptions.Item label="start_date">
                  {acco_info.start_date}
                </Descriptions.Item>
                <Descriptions.Item label="end_date">
                  {acco_info.end_date}
                </Descriptions.Item>
                <Descriptions.Item label="beds">
                  {acco_info.beds}
                </Descriptions.Item>
                <Descriptions.Item label="bedrooms">
                  {acco_info.bedrooms}
                </Descriptions.Item>
                <Descriptions.Item label="carparks">
                  {acco_info.carparks}
                </Descriptions.Item>
              </Descriptions>
            </Row>
          </QueueAnim>
        </div>
      </div>
    );
  }
}

export default Orderaccommodationdetails;
