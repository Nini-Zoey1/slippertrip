import React from "react";
import {
  Carousel,
  Descriptions,
  Row,
  Col,
  Divider,
  Button,
  Spin,
  Drawer,
  message,
  Alert
} from "antd";
import axios from "axios";
import QueueAnim from "rc-queue-anim";
import "./accommodationdetails.css";
import { apiUrl } from "../../config.json";
import MapSection from "../../components/mapSection";
import OrderForm from "../../components/orderForm";
import Comments from "../../components/comment";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));
// const endpointUrl = apiUrl + "/order";

//import NaviBar from "./NaviBar";
class accommodationdetails extends React.Component {
  state = {
    loading: false,
    host_user: {},
    acco_info: {},
    ModalVisible: false,
    orderLoading: false,
    drawerVisable: false,
    dateMessage: ""
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(
        `http://127.0.0.1:8000/api/hotel/${this.props.match.params.id}/view/`
      )
      .then(res => {
        console.log("details res", res);
        const acco_info = { ...res.data.data.acco_info };
        const host_user = { ...res.data.data.host_user };
        console.log("acco_info", acco_info);
        console.log("host_user", host_user);

        this.setState({ acco_info, host_user, loading: false });
      })
      .catch(error => console.log(error));
  }
  orderQuery = {
    acco: 1,
    u_title: "2",
    u_lastname: "li",
    u_firstname: "xu",
    u_people: 2,
    u_telephone: "1234567890",
    u_email: "xulizoey@163.com",
    date_in: "2019-10-16",
    date_out: "2019-10-17",
    total_price: 999
  };
  duration = this.props.match.params.duration;
  handleOrder = () => {
    // console.log("duration", duration);
    // console.log("id", this.props.match.params.id);
    // axios.post(endpointUrl, this.auery).then(res => console.log(res));
  };

  showDrawer = () => {
    if (!localStorage.getItem("token")) {
      message.warning("Please Login/ Register...");
      const fromurl = this.props.location.pathname.replace(/\//g, "~");
      // console.log("fromurl", fromurl);
      this.props.history.push(`/login/${fromurl}`);
      return;
    }
    this.setState({
      drawerVisable: true
    });
  };

  onClose = () => {
    this.setState({
      drawerVisable: false
    });
  };
  handleDateMessage = msg => {
    this.setState({ dateMessage: msg });
  };
  handleNormailseScore = score => {
    if (score) {
      return parseFloat((score / 20).toFixed(2));
    }
  };

  render() {
    // const accId = this.props.match.params.id;
    const host_user = this.state.host_user;
    const acco_info = this.state.acco_info;
    return (
      // <div
      //   style={{
      //     maxWidth: 900,
      //     backgroundColor: "#eee",
      //     border: 2,
      //     marginleft: "auto",
      //     marginRight: "auto"
      //   }}
      // ></div>

      <div>
        <Row style={{ width: "100%" }} className="accdetailimage">
          <p className="titlede">Sydney Hotels and Accommodation Details</p>
        </Row>
        {this.state.loading ? (
          <Row type="flex" justify="center" gutter={40}>
            <Col>
              <Spin size="large" />
            </Col>
          </Row>
        ) : (
          <div className="paper">
            <QueueAnim delay={600}>
              <Row key="o1" gutter={40} type="flex" justify="center">
                <Col sm={24} md={12}>
                  <Carousel>
                    <div>
                      <img
                        style={{ width: "100%", borderRadius: 10 }}
                        src={acco_info.pic_url}
                        alt=""
                      />
                    </div>
                    <div>
                      <img
                        style={{ width: "100%", borderRadius: 10 }}
                        src="https://a0.muscache.com/im/pictures/3f8ec93f-6d07-4d5c-8635-edfc3adb7e77.jpg?aki_policy=xx_large"
                        alt=""
                      />
                    </div>
                    <div>
                      <img
                        style={{ width: "100%", borderRadius: 10 }}
                        src="https://a0.muscache.com/im/pictures/79fff1ed-262e-4be6-b0a9-8842cdcefc83.jpg?aki_policy=xx_large"
                        alt=""
                      />
                    </div>
                  </Carousel>
                </Col>
                <Col sm={24} md={10}>
                  <QueueAnim delay={500} type="bottom">
                    <Row key="r1">
                      <Descriptions
                        title="Host Info"
                        // layout="vertical"
                        bordered
                      >
                        <Descriptions.Item label="Email" span={3}>
                          {host_user.email}
                        </Descriptions.Item>
                        <Descriptions.Item label="Username" span={3}>
                          {host_user.username}
                        </Descriptions.Item>
                        <Descriptions.Item label="Phone" span={3}>
                          {host_user.phone}
                        </Descriptions.Item>
                      </Descriptions>
                    </Row>
                    <Row
                      key="r2"
                      type="flex"
                      justify="center"
                      align="middle"
                      style={{ height: 150 }}
                    >
                      <Button
                        size="large"
                        type="primary"
                        onClick={this.showDrawer}
                      >
                        Order
                      </Button>
                      <Drawer
                        width="600px"
                        title="Order details"
                        placement="right"
                        closable={false}
                        onClose={this.onClose}
                        visible={this.state.drawerVisable}
                      >
                        {" "}
                        {this.state.dateMessage && (
                          <Alert
                            message={this.state.dateMessage}
                            type="info"
                            closable
                            banner="true"
                            // onClose={onClose}
                          />
                        )}
                        <Row type="flex" justify="center">
                          <Col sm={20}>
                            <OrderForm
                              handleDateMessage={this.handleDateMessage}
                              history={this.props.history}
                              acco={acco_info.id}
                              price={acco_info.price}
                            />
                          </Col>
                        </Row>
                      </Drawer>

                      {/* <Button
                        onClick={this.handleOrder}
                        type="primary"
                        size="large"
                        loading={this.state.orderLoading}
                      >
                        Order
                      </Button> */}
                    </Row>
                    <Row />
                  </QueueAnim>
                </Col>
              </Row>
              <Divider style={{ height: 2, color: "gray" }} />

              <Row key="o2" gutter={20} type="flex" justify="center">
                <Col sm={24} md={14}>
                  <Row>
                    <Col>
                      <Descriptions
                        size="middle"
                        title="Accommodation Info"
                        bordered
                        column={2}
                      >
                        <Descriptions.Item label="Title" span={2}>
                          {acco_info.title}
                        </Descriptions.Item>
                        {/* <Descriptions.Item label="addr_street" span={2}>
                  {acco_info.addr_street}
                </Descriptions.Item> */}

                        <Descriptions.Item label="Price">
                          {acco_info.price}
                        </Descriptions.Item>
                        <Descriptions.Item label="Acco_score">
                          {this.handleNormailseScore(
                            parseFloat(acco_info.acco_score)
                          )}
                        </Descriptions.Item>
                        <Descriptions.Item label="Addr_city">
                          {acco_info.addr_city}
                        </Descriptions.Item>
                        <Descriptions.Item label="Capacity">
                          {acco_info.people}
                        </Descriptions.Item>
                        <Descriptions.Item label="Start_date">
                          {acco_info.start_date}
                        </Descriptions.Item>
                        <Descriptions.Item label="End_date">
                          {acco_info.end_date}
                        </Descriptions.Item>
                        <Descriptions.Item label="Beds">
                          {acco_info.beds}
                        </Descriptions.Item>
                        <Descriptions.Item label="Bedrooms">
                          {acco_info.bedrooms}
                        </Descriptions.Item>
                        <Descriptions.Item label="Update_time" span={2}>
                          {acco_info.update_time}
                        </Descriptions.Item>
                        <Descriptions.Item label=" Description">
                          {acco_info.description}
                        </Descriptions.Item>
                      </Descriptions>
                    </Col>
                    <Col>
                      <Comments />
                    </Col>
                  </Row>
                </Col>

                <Col sm={24} md={8}>
                  <MapSection
                    lat={this.state.acco_info.latitude}
                    lng={this.state.acco_info.longtitude}
                  />
                </Col>
              </Row>
            </QueueAnim>
          </div>
        )}
      </div>
    );
  }
}

export default accommodationdetails;
