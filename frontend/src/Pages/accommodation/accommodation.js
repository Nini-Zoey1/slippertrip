import React from "react";
import {
  BackTop,
  Row,
  Col,
  Tag,
  Spin,
  notification,
  Modal,
  message
} from "antd";
import QueueAnim from "rc-queue-anim";
import Texty from "rc-texty";
import "rc-texty/assets/index.css";
import "./accommodation.css";
import { getAccList } from "../../fakeService/fakeAcclist";
import { Button } from "antd";
import queryString from "query-string";
import AccommodationList from "../../components/AccommodationList";
import { apiUrl } from "../../config.json";
import AccommodationSearch from "../../components/accommodationSearch";
import axios from "axios";
import _ from "lodash";
import moment from "moment";
localStorage.getItem("token") &&
  (axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token"));

const LikeApiUrl = "http://127.0.0.1:8000/api/hotel/save/";

//import NaviBar from "./NaviBar";
const endpointUrl = apiUrl + "/hotel/search/";
const aiSearchingUrl = "http://127.0.0.1:8000/api/hotel/keepsearching/";
class accommodation extends React.Component {
  state = {
    accList: [],
    // fistAIloading: false,
    loading: false,
    currentStop: 0,
    sortCloumn: { path: "title", order: "asc" },
    mapSection: null,
    likedList: [],
    aiStarted: false,
    keepLoading: false,
    filters: [],
    duration: 0,
    // ModalVisible: false,
    perferences: []
  };
  componentDidMount() {
    // const query = queryString.parse(this.props.location.search);
    // console.log("location:", this.props.location);
    // console.log("query:", query);
    // const startDate = moment(query.start_date);
    // const endDate = moment(query.end_date);
    // const duration = endDate.diff(startDate, "days") + 1;
    // this.setState({ duration });
    // console.log("duration", endDate.diff(startDate, "days") + 1);
    // console.log("startDate", startDate);
    // let newQuery = { ...query };
    // newQuery.people = parseInt(query.people);
    // console.log("parseInt-query:", newQuery);
    // console.log("query:", query);
    console.log("search url", endpointUrl + this.props.location.search);
    this.setState({ loading: true });
    axios
      .get(endpointUrl + this.props.location.search)
      // .post(endpointUrl, newQuery)
      .then(res => {
        console.log(res);
        res.data.data
          ? this.setState({ accList: res.data.data.acco_list, loading: false })
          : this.setState({ accList: [], loading: false });
      })
      .catch(error => console.log(error));
    // if (localStorage.getItem("token")) {
    //   axios
    //     .get("http://127.0.0.1:8000/api/hotel/save/")
    //     .then(res => {
    //       console.log("save-list", res);
    //       const llres = res.data.data.saved_acco_list;
    //       if (llres.length === 0) return;
    //       let likedList = [];
    //       for (let i = 0; i < llres.length; i++) {
    //         likedList.push(llres[i][0].id);
    //       }
    //       this.setState({ likedList });
    //     })
    //     .catch(error => console.log(error));
    // }

    // this.setState({ loading: false }, () => {
    //   const accList = getAccList();
    //   this.setState({ accList });
    // });
    // console.log(getAccList());
    // const accList = getAccList();
    // this.setState({ accList });
    // const mapUrl =
    //   "https://image.maps.api.here.com/mia/1.6/mapview?poi=52.5%2C13.4%2C41.9%2C12.5%2C51.5%2C-0.1%2C48.9%2C2.3%2C40.4%2C-3.7&poitxs=16&poitxc=black&poifc=yellow&app_id=XKUb5CUZ2pHJY55Z2lYD&app_code=LvybwTnENVGvTcpVaY9WLg";
    // // axios.get(mapUrl).then(res => console.log(res));
    // console.log("mapSection", this.state.mapSection);
    // fetch(mapUrl)
    //   .then(res => res.json())
    //   .then(res => console.log(res))
    //   .catch(error => console.log(error));
  }
  enterLoading = () => {
    this.setState({ loading: true });
    //api
  };

  stopLoading = () => {
    this.setState({ loading: false });
  };

  handleCurrentStop = currentStop => {
    this.setState({ currentStop });
    console.log(this.state.currentStop);
  };

  handsorting = path => {
    const sortCloumn = { ...this.state.sortCloumn };
    if (sortCloumn.path === path) {
      sortCloumn.order = sortCloumn.order === "asc" ? "desc" : "asc";
    } else {
      sortCloumn.path = path;
      sortCloumn.order = "asc";
    }
    let accList = [...this.state.accList];
    const sorted = _.sortBy(
      accList,
      [this.state.sortCloumn.path],
      [this.state.sortCloumn.order]
    );
    this.setState({ sortCloumn, accList: sorted });
  };
  handleLikedListAdd = ele => {
    let likedList = [...this.state.likedList];
    likedList.push(ele);
    this.setState({ likedList });
  };
  handleLikedListRemove = value => {
    let likedList = [...this.state.likedList];
    let newlist = likedList.filter(ele => ele !== value);
    this.setState({ likedList: newlist });
  };
  // handleCheckLikedOrNot = id => {
  //   const likedList = [...this.state.likedList];
  //   return likedList.includes(id);
  // };

  handleBusicSearch = values => {
    console.log("search button", values);
    const queryString = `?addr_city=${values.addr_city}&people=${
      values.people
    }&start_date=${values.start_date}&end_date=${values.end_date} `;
    console.log("queryString", queryString);
    window.location = `/acclist/${queryString}`;

    // console.log("search url", endpointUrl + queryString);
    // this.setState({ loading: true });
    // axios
    //   .get(endpointUrl + this.props.location.search)
    //   // .post(endpointUrl, newQuery)
    //   .then(res => {
    //     console.log(res);
    //     res.data.data
    //       ? this.setState({ accList: res.data.data.acco_list, loading: false })
    //       : this.setState({ accList: [], loading: false });
    //   })
    //   .catch(error => console.log(error));

    // const queryString = `?addr_city=${values.addr_city}&people=${
    //   values.people
    // }&start_date=${values.start_date}&end_date=${values.end_date} `;
    // console.log("queryString", queryString);
    // // console.log(this.props);
    // this.props.history.push(`/acclist/${queryString}`);
  };
  handleLiked = id => {
    if (!localStorage.getItem("token")) {
      message.warning("Please Login/ Register...");
      return;
    }
    console.log(id);
    const alreadyLiked = this.state.likedList.includes(id);
    if (alreadyLiked) {
      this.handleLikedListRemove(id);
      axios
        .delete(`http://127.0.0.1:8000/api/hotel/${id}/unsave/`, {
          acco_id: id
        })
        .then(res => console.log("un-like-res", res))
        .catch(error => console.log(error));
    } else {
      this.handleLikedListAdd(id);
      axios
        .post(LikeApiUrl, { acco_id: id })
        .then(res => console.log("add-like-res", res))
        .catch(error => console.log(error));
    }
  };

  handleStartSearching = () => {
    if (!localStorage.getItem("token")) {
      message.warning("Please Login/ Register...");
      return;
    }
    // if (this.state.perferences.length === 0) {
    //   this.setState({ ModalVisible: true });
    // } else
    if (this.state.likedList.length === 0) {
      this.openNotification();
    } else {
      this.setState({ fistAIloading: true });
      // console.log("length", this.state.likedList.length);
      console.log(
        "ai-url",
        aiSearchingUrl +
          this.props.location.search.trim() +
          `&latest_saved_acco_id=${this.state.likedList.slice(-1)[0]}`
      );
      axios
        .get(
          aiSearchingUrl +
            this.props.location.search.trim() +
            `&latest_saved_acco_id=${this.state.likedList.slice(-1)[0]}`
        )
        .then(res => {
          console.log("ai-res", res);
          const accList = [...res.data.data.acco_list];
          const filters = [...res.data.data["filters:"]];
          // console.log("res.data.data.filters", res.data.data["filters:"]);
          this.setState({
            accList,
            filters,
            aiStarted: true,
            fistAIloading: false
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            accList: [],
            filters: [],
            aiStarted: true,
            fistAIloading: false
          });
        });
    }
  };
  startSearchingCallApi = () => {};

  handleKeepSearching = () => {
    if (this.state.likedList.length === 0) {
      this.openNotification();
    } else {
      this.setState({ keepLoading: true });
      // console.log("length", this.state.likedList.length);
      axios
        .get(
          aiSearchingUrl +
            this.props.location.search.trim() +
            `&latest_saved_acco_id=${this.state.likedList.slice(-1)[0]}`
        )
        .then(res => {
          console.log("ai-res", res);
          const accList = [...res.data.data.acco_list];
          const filters = [...res.data.data["filters:"]];
          this.setState({ keepLoading: false });

          // console.log("res.data.data.filters", res.data.data["filters:"]);
          this.setState({ accList, filters });
        })
        .catch(error => console.log(error));
    }
  };

  openNotification = () => {
    const key = `open${Date.now()}`;
    const btn = (
      <Button
        type="primary"
        size="small"
        onClick={() => notification.close(key)}
      >
        got it
      </Button>
    );
    notification.open({
      message: "Please tell us something...",
      description:
        "just click the 'LIKE' button if you like the place... Our AI algorithoms are working on your preferences! ",
      btn,
      key,
      duration: 0
    });
  };
  // handleModalLeft = () => {
  //   let perferences = [...this.state.perferences];
  //   perferences.push("1");
  //   this.handleLikedListAdd(115);
  //   axios
  //     .post(LikeApiUrl, { acco_id: 115 })
  //     .then(res => console.log("add-like-res", res))
  //     .catch(error => console.log(error));

  //   this.setState({ perferences, ModalVisible: false });
  // };
  // handleModalRight = () => {
  //   let perferences = [...this.state.perferences];
  //   perferences.push("2");
  //   this.handleLikedListAdd(129);
  //   axios
  //     .post(LikeApiUrl, { acco_id: 129 })
  //     .then(res => console.log("add-like-res", res))
  //     .catch(error => console.log(error));

  //   this.setState({ perferences, ModalVisible: false });
  // };

  render() {
    const defaultQuery = queryString.parse(this.props.location.search);
    // console.log("query", query);
    return (
      <React.Fragment>
        <BackTop />

        <Row style={{ width: "100%" }} className="accimage">
          <p className="titlebig">Sydney Hotels and Accommodation</p>
        </Row>
        {this.state.loading ? (
          <Row type="flex" justify="center" gutter={40}>
            <Col>
              <Spin size="large" />
            </Col>
          </Row>
        ) : (
          <div
            style={{
              background: "#fff",
              margin: 0,
              minHeight: 280
            }}
          >
            <QueueAnim type="bottom" delay={500}>
              {!this.state.aiStarted ? (
                <Row
                  key="11"
                  type="flex"
                  justify="center"
                  style={{
                    padding: 20,
                    marginTop: "20px",
                    marginBottom: 30,
                    // backgroundColor: "rgb(245	,247,	250	)"
                    backgroundColor: "rgb(141,111,78)"
                  }}
                >
                  <QueueAnim type="left" delay={1000}>
                    <Button
                      key="btn"
                      // type="primary"
                      loading={this.state.fistAIloading}
                      onClick={this.handleStartSearching}
                      style={{
                        borderRadius: 30,
                        color: "#774a17",
                        backgroundColor: "rgb(245	,247,	250	)"
                      }}
                    >
                      Start Personalized Recommendation Engine
                    </Button>
                  </QueueAnim>
                  {/* <Modal
                    width="700px"
                    title="Which style do you perfer... Please select one"
                    visible={this.state.ModalVisible}
                    // onOk={this.handleOk}
                    // confirmLoading={this.state.orderLoading}
                    // onCancel={this.handleCancel}
                  >
                    <Row gutter={30}>
                      <Col sm={24} md={12}>
                        <img
                          onClick={this.handleModalLeft}
                          alt="675"
                          style={{ width: 300, height: 200, cursor: "pointer" }}
                          src="https://a0.muscache.com/im/pictures/aefc3318-de1b-4b6b-bf55-0718ddcd849c.jpg?aki_policy=large"
                        />
                      </Col>
                      <Col sm={24} md={12}>
                        <img
                          alt="129"
                          style={{ width: 300, height: 200, cursor: "pointer" }}
                          onClick={this.handleModalRight}
                          src="https://a0.muscache.com/im/pictures/6432edee-1993-4927-b24c-2bfe41704ee6.jpg?aki_policy=large"
                        />
                      </Col>
                    </Row>
                  </Modal> */}
                </Row>
              ) : (
                <Row
                  key="1"
                  type="flex"
                  justify="center"
                  style={{
                    padding: 20,
                    marginTop: "20px",
                    marginBottom: 30,
                    // backgroundColor: "rgb(245	,247,	250	)"
                    backgroundColor: "rgb(141,111,78)"
                  }}
                >
                  <Col>
                    <QueueAnim type="left" delay={300}>
                      <Texty
                        style={{ marginRight: 20, display: "inline-block" }}
                        key="l1"
                        type="bottom"
                        mode="random"
                        delay={500}
                      >
                        Personalized Recommendation
                      </Texty>
                      {this.state.filters &&
                        this.state.filters.map((item, index) => (
                          <Tag
                            style={{ marginRight: 10 }}
                            color="#774a17"
                            key={index}
                          >
                            {item}
                          </Tag>
                        ))}

                      <Button
                        key="btn"
                        // type="primary"
                        loading={this.state.keepLoading}
                        onClick={this.handleKeepSearching}
                        style={{
                          borderRadius: 30,
                          color: "#774a17",
                          backgroundColor: "rgb(245	,247,	250	)"
                        }}
                      >
                        Keep Searching
                      </Button>
                    </QueueAnim>
                  </Col>
                </Row>
              )}

              <Row type="flex" justify="center" gutter={30}>
                <QueueAnim type="right" delay={300}>
                  <Col key="r1" span={3}>
                    {/* <Button onClick={this.stopLoading} /> */}
                    <Button onClick={() => this.handsorting("price")}>
                      Sorting by price
                    </Button>
                  </Col>
                  <Col key="r2" span={3}>
                    <Button onClick={() => this.handsorting("acco_score")}>
                      sorting by rating
                    </Button>
                  </Col>
                  <Col key="r3" span={18}>
                    <AccommodationSearch
                      defaultQuery={defaultQuery}
                      onSubmit={this.handleBusicSearch}
                    />
                  </Col>
                </QueueAnim>
              </Row>
              <Row key="2" type="flex" justify="center">
                <Col span={16}>
                  <div style={{ padding: 15 }}>
                    <AccommodationList
                      duration={this.state.duration}
                      items={this.state.accList}
                      likedList={this.state.likedList}
                      handleCurrentStop={this.handleCurrentStop}
                      handleLiked={this.handleLiked}
                      // handleLikedListAdd={this.handleLikedListAdd}
                      // handleLikedListRemove={this.handleLikedListRemove}
                      // handleCheckLikedOrNot={this.handleCheckLikedOrNot}
                    />
                  </div>
                </Col>
              </Row>
            </QueueAnim>
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default accommodation;
