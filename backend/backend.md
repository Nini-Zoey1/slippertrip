# backend venv description

venv base: python 3.7

others interpreter: requirements.txt

TIPs:

1. File "/Desktop/backend/slippertrip/venv/lib/python3.7/site-packages/django/db/backends/mysql/base.py", line 34

   > ```python
   > version = Database.version_info
   > # if version < (1, 3, 13):
   > #     raise ImproperlyConfigured('mysqlclient 1.3.13 or newer is required; you have %s.' % Database.__version__)
   > ```

2. File "/backend/slippertrip/venv/lib/python3.7/site-packages/django/db/backends/mysql/operations.py", line 145

   > ```python
   > if query is not None:
   >     # query = query.decode(errors='replace')
   >     query = 'replace'
   > return query
   > ```

   

