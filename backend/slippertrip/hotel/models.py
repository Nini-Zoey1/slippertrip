from django.db import models

from django.db import models
from django.conf import settings
from base_model.base_model import BaseModel


class AccoInfo(BaseModel):
    """
    Hotel basic info
    """
    host_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=15)
    title = models.CharField(max_length=100)
    addr_street = models.CharField(max_length=100, null=True, blank=True)
    addr_city = models.CharField(max_length=30, null=True, blank=True)
    people = models.IntegerField()
    start_date = models.DateField(default="2019-07-01")
    end_date = models.DateField(default="2020-12-31")
    reserved_date = models.TextField(null=True, blank=True)
    price = models.IntegerField()
    acco_score = models.DecimalField(max_digits=4, decimal_places=1, default=99.9)
    reviews_number = models.IntegerField(default=0)
    beds = models.IntegerField()
    bedrooms = models.IntegerField()
    bathrooms = models.IntegerField()
    latitude = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True)
    longtitude = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    air_conditioner = models.BooleanField(default=True)
    tv = models.BooleanField(default=True)
    wifi = models.BooleanField(default=True)
    kitchen = models.BooleanField(default=True)
    shampoo = models.BooleanField(default=True)
    self_check_in = models.BooleanField(default=True)
    hair_dryer = models.BooleanField(default=True)
    freezer = models.BooleanField(default=True)
    accommodates = models.IntegerField(null=True, blank=True)
    reviews_per_month = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    availability_365 = models.IntegerField(null=True, blank=True)
    minimum_nights = models.IntegerField(null=True, blank=True)
    maximum_nights = models.IntegerField(null=True, blank=True)
    calculated_host_listings_count = models.IntegerField(null=True, blank=True)
    cluster = models.IntegerField(null=True, blank=True)
    pic_url = models.CharField(max_length=254, default='https://a0.muscache.com/4ea/air/v2/pictures/40033023-8228-4b2b-a519-4f67a849e457.jpg?t=r:w2500-h1500-sfit,e:fjpg-c90')
    pic1 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)
    pic2 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)
    pic3 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)
    pic4 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)
    pic5 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)
    pic6 = models.ImageField(upload_to='hotel_pics', null=True, blank=True)

    class Meta:
        db_table = 'acco_info'
        verbose_name = 'Acco Info'
        verbose_name_plural = verbose_name


class SavedAcco(models.Model):
    """
    The saved accommodations which the user saved
    save_status BooleanField True for yes, False for no, default=True
    """
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    acco = models.ForeignKey('hotel.AccoInfo', on_delete=models.CASCADE)
    save_status = models.BooleanField(default=True)

    class Meta:
        unique_together = ('user', 'acco',)
        db_table = 'savedacco'
        verbose_name = 'SavedAcco'
        verbose_name_plural = verbose_name
