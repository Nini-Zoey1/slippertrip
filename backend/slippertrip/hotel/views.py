import json
import requests

from decimal import *

from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated

from .serializer import AccoInfoSerializer, SavedSerializer
from .models import AccoInfo, SavedAcco
from user.models import User
from order.models import Order
from user.serializer import UserSerializer
from common_response.common_response import common_response, StatusCode
from utils.formatter import replace_strings
from utils.check_date_available import check_date_available
from machine_learning.function import final


class AccoInfoAPIView(RetrieveUpdateAPIView):
    """
    Get and Post acco info by host_user
    """
    permission_classes = (IsAuthenticated,)
    acco_serializer_class = AccoInfoSerializer
    user_serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        """
        Get posted acco list
        :param request: token
        :param args:
        :param kwargs:
        :return:
        """
        host_user = request.user.id
        dataset = {
            'host_user': {'username': request.user.username, 'phone': request.user.phone, 'email': request.user.email}}
        try:
            dataset['acco_info'] = list(AccoInfo.objects.filter(host_user=host_user, is_delete=False)[0:100].values())
            return common_response(code=StatusCode.SUCCESS, data=dataset)
        except Exception as e:
            print(e)
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="No posted acco of this user!",
                                   data=dataset)

    def post(self, request):
        """
        Post new acco info
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        host_user = request.user.id
        dataset = {
            'host_user': {'username': request.user.username, 'phone': request.user.phone, 'email': request.user.email}}
        acco_info = request.data
        acco_info['host_user'] = host_user
        acco_addr = str(acco_info['addr_street']) + " " + str(acco_info['addr_city']) + " NSW" + " AU"
        acco_addr = replace_strings(acco_addr, '%20')
        r = (requests.get(
            "https://geocoder.api.here.com/6.2/geocode.json?searchtext=" + acco_addr + "&app_id=XKUb5CUZ2pHJY55Z2lYD&app_code=LvybwTnENVGvTcpVaY9WLg&gen=9")).text
        r = json.loads(r)
        geocode = r['Response']['View'][0]['Result'][0]['Location']['NavigationPosition'][0]
        acco_info['latitude'] = Decimal(geocode['Latitude']).quantize(Decimal('0.0000000'))
        acco_info['longtitude'] = Decimal(geocode['Longitude']).quantize(Decimal("0.0000000"))
        serializer = self.acco_serializer_class(data=acco_info)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        dataset['acco_info'] = serializer.data
        return common_response(code=StatusCode.SUCCESS, data=dataset)


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny, ])
def get_acco_info(request, acco_id):
    """
    get specific acco info
    :param request:
    :param acco_id:
    :return:
    """
    try:
        acco = list(AccoInfo.objects.filter(id=int(acco_id), is_delete=False).values())[0]
        # print(acco)
        host_user_info = list(User.objects.filter(id=int(acco['host_user_id'])).values())[0]
        host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                     'phone': host_user_info['phone']}
        dataset = {'host_user': host_user, 'acco_info': acco}
        return common_response(code=StatusCode.SUCCESS, data=dataset)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect AccoID!")


@api_view(http_method_names=['PUT'])
@permission_classes([IsAuthenticated, ])
def edit_acco_info(request, acco_id):
    """
    Edit specific acco
    :param request:
    :param acco_id:
    :return:
    """
    host_user_id = request.user.id
    try:
        acco = AccoInfo.objects.get(id=int(acco_id), is_delete=False)
        if host_user_id != acco.host_user.id:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Mismatched user and acco!")
        else:
            serializer = AccoInfoSerializer(
                instance=acco, data=eval(bytes.decode(request.body)), partial=True
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()
            host_user_info = list(User.objects.filter(id=host_user_id).values())[0]
            host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                         'phone': host_user_info['phone']}
            dataset = {'host_user': host_user, 'acco_info': serializer.data}
            return common_response(code=StatusCode.SUCCESS, data=dataset)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect acco_id!")


@api_view(http_method_names=['DELETE'])
@permission_classes([IsAuthenticated, ])
def delete_acco(request, acco_id):
    """
    Delete specific acco
    :param request:
    :param acco_id:
    :return:
    """
    host_user = request.user.id
    try:
        AccoInfo.objects.get(host_user=host_user, id=int(acco_id)).delete()
        return common_response(code=StatusCode.SUCCESS)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect AccoID!")


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny, ])
def search_acco(request):
    """
    normal search from homepage
    :param request:
    :return:
    """
    addr_city = request.GET.get('addr_city')
    people = int(request.GET.get('people'))
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    filters = []
    dataset = {'filters': filters}
    try:

        accos = list(AccoInfo.objects.filter(addr_city=addr_city, people__gte=people, start_date__lte=start_date,
                                             end_date__gte=end_date, is_delete=False)[:120].values())
        data = []
        print(accos)
        for acco in accos:
            if check_date_available(acco['start_date'], acco['end_date'], acco['reserved_date'], start_date, end_date) is True:
                data.append(acco)
            else:
                continue
        if len(data) > 0:
            # print(data)
            dataset['acco_list'] = data
            return common_response(code=StatusCode.SUCCESS, data=dataset)
        else:
            print(data)
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Search nothing!")
    except Exception as e:
        print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Search nothing!")


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny, ])
def keep_searching(request):
    """
    AI searching
    :param request:
    :return:
    """
    addr_city = request.GET.get('addr_city')
    # people = int(request.GET.get('people'))
    # start_date = request.GET.get('start_date')
    # end_date = request.GET.get('end_date')
    latest_saved_id = int(request.GET.get('latest_saved_acco_id'))
    try:
        saved_acco_info = AccoInfo.objects.get(id=latest_saved_id)
        price = saved_acco_info.price
        bedrooms = saved_acco_info.bedrooms
        bathrooms = saved_acco_info.bathrooms
        acco_score = saved_acco_info.acco_score
        reviews_number = saved_acco_info.reviews_number
        cluster = int(saved_acco_info.cluster)
        filters = []
        if price >= 300:
            filters.append('FANCY')
        else:
            filters.append('ECONOMY')

        if bedrooms >= 3:
            filters.append('COSY')
        else:
            filters.append('BUDGET')

        if bathrooms >= 2:
            filters.append('UNIQUE')
        else:
            filters.append('WARM')

        if reviews_number >= 20:
            filters.append('HOT')
        else:
            filters.append('QUIET')

        if acco_score >= 9:
            filters.append('NEAT')
        else:
            filters.append('ACCEPTABLE')

        # accos = list(AccoInfo.objects.filter(addr_city=addr_city, people__gte=people, start_date__lte=start_date,
        #                                      end_date__gte=end_date, price__lte=price + 30, price__gte=price - 30,
        #                                      bedrooms=bedrooms, bathrooms=bathrooms, acco_score__gte=acco_score,
        #                                      is_delete=False)[:120].values())

        input_data = {'id': latest_saved_id, 'addr_city': addr_city, 'cluster': cluster}
        acco_list = []
        output_data = final(input_data)
        acco_list.extend(output_data)
        data = {'filters:': filters}
        # print(output_data)
        # print(len(output_data))
        if len(acco_list) > 0:
            data['acco_list'] = acco_list
            return common_response(code=StatusCode.SUCCESS, data=data)
        else:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Search nothing!", data=data)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Search nothing!")


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny, ])
def get_acco_reviews(request, acco_id):
    """
    get all reviews of one specific acco
    :param request:
    :param acco_id:
    :return:
    """
    try:
        orders_in_acco = list(
            Order.objects.filter(acco=int(acco_id), is_delete=False).exclude(review=None).values("acco_id",
                                                                                                 "u_firstname",
                                                                                                 "review"))
        return common_response(code=StatusCode.SUCCESS, data=orders_in_acco)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="No reviews!")


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny, ])
def check_date(request, acco_id):
    """
    check a couple of dates aviliable
    :param request:
    :param acco_id:
    :return:
    """
    date_in = request.GET.get('date_in')
    date_out = request.GET.get('date_out')
    acco = AccoInfo.objects.get(id=int(acco_id))
    start_date = acco.start_date
    end_date = acco.end_date
    reserved_date = acco.reserved_date
    if check_date_available(start_date, end_date, reserved_date, date_in, date_out) is True:
        return common_response(code=StatusCode.SUCCESS, message="Dates available!")
    else:
        return common_response(code=StatusCode.ERROR, message="Dates not available!")


class SavedAccoAPIView(RetrieveUpdateAPIView):
    """
    get or add user saved list
    """
    permission_classes = (IsAuthenticated,)
    user_serializer_class = UserSerializer
    saved_serializer_class = SavedSerializer

    def get(self, request, *args, **kwargs):
        """
        Get user saved list
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user.id
        data = {'host_user_info': {'id': user, 'username': request.user.username, 'phone': request.user.phone,
                                   'email': request.user.email}}
        try:
            saved_list = SavedAcco.objects.filter(user=user, save_status=True)
            acco_list = []
            for acco in saved_list:
                acco_info = list(AccoInfo.objects.filter(id=acco.acco.id).values())
                acco_list.append(acco_info)
            data['saved_acco_list'] = acco_list
            return common_response(code=StatusCode.SUCCESS, data=data)
        except Exception as e:
            # print(e)
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="No saved acco of this user!",
                                   data=data)

    def post(self, request):
        """
        Add saved acco into user saved list
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user.id
        acco_id = request.data['acco_id']
        try:
            acco = AccoInfo.objects.get(id=acco_id)
            try:
                SavedAcco.objects.get(acco=acco_id, user=user)
                return common_response(code=StatusCode.SUCCESS)
            except Exception as e:
                # print(e)
                saved_data = {'user': user, 'acco': acco.id, 'saved_status': True}
                serializer = self.saved_serializer_class(data=saved_data)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                return common_response(code=StatusCode.SUCCESS)
        except Exception as e:
            # print(e)
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect acco_id!")


@api_view(http_method_names=['DELETE'])
@permission_classes([IsAuthenticated, ])
def unsave(request, acco_id):
    """
    cancel saved acco from saved list
    :param acco_id:
    :param request:
    :return:
    """
    user = request.user.id
    try:
        SavedAcco.objects.get(user=user, acco=int(acco_id)).delete()
        return common_response(code=StatusCode.SUCCESS)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="User not save this acco!")
