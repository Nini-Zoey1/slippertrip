from django.contrib import admin
from .models import AccoInfo, SavedAcco

admin.site.register(AccoInfo)
admin.site.register(SavedAcco)
