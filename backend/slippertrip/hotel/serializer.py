from rest_framework import serializers
from .models import AccoInfo, SavedAcco


class AccoInfoSerializer(serializers.ModelSerializer):
    create_time = serializers.ReadOnlyField()
    update_time = serializers.ReadOnlyField()

    class Meta(object):
        model = AccoInfo
        fields = ('id', 'create_time', 'update_time', 'is_delete', 'host_user',
                  'title', 'addr_street', 'addr_city', 'people', 'start_date', 'end_date', 'reserved_date',
                  'price', 'acco_score', 'beds', 'bedrooms', 'bathrooms', 'latitude', 'longtitude',
                  'description', 'air_conditioner', 'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in',
                  'hair_dryer', 'freezer', 'pic1', 'pic2', 'pic3', 'pic4', 'pic5', 'pic6', 'reviews_number', 'pic_url',
                  'accommodates', 'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                  'calculated_host_listings_count', 'cluster')


class SavedSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = SavedAcco
        fields = ('id', 'user', 'acco', 'save_status')
