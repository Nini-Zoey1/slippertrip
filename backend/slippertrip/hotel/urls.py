"""slippertrip URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from django.conf.urls import url

from .views import AccoInfoAPIView, get_acco_info, edit_acco_info, delete_acco, search_acco, get_acco_reviews, check_date, SavedAccoAPIView, keep_searching, unsave

urlpatterns = [
    path('', AccoInfoAPIView.as_view()),
    path('save/', SavedAccoAPIView.as_view()),
    url(r'^(?P<acco_id>.*)/unsave/', unsave),
    path('search/', search_acco),
    path('keepsearching/', keep_searching),
    url(r'^(?P<acco_id>.*)/view/', get_acco_info),
    url(r'^(?P<acco_id>.*)/edit/', edit_acco_info),
    url(r'^(?P<acco_id>.*)/delete/', delete_acco),
    url(r'^(?P<acco_id>.*)/reviews/', get_acco_reviews),
    url(r'^(?P<acco_id>.*)/check/', check_date),

]
