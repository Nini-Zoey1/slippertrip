from datetime import datetime
from decimal import Decimal

from django.db.models import Avg
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated

from common_response.common_response import common_response, StatusCode
from user.models import User
from hotel.serializer import AccoInfoSerializer
from hotel.models import AccoInfo
from utils.check_date_available import check_date_available
from .serializer import OrderSerializer
from .models import Order


class OrderAPIView(RetrieveUpdateAPIView):
    """
    Create/ Review order info
    """
    permission_classes = (IsAuthenticated,)
    order_serializer_class = OrderSerializer
    acco_serializer_class = AccoInfoSerializer

    def get(self, request, *args, **kwargs):
        """
        Review reserved oder list
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        u_user_id = request.user.id
        dataset = {'u_user': {'u_user_id': u_user_id}}
        try:
            orders = list(Order.objects.filter(u_user=u_user_id, is_delete=False).values())
            order_list = []
            for order in orders:
                acco = list(AccoInfo.objects.filter(id=int(order['acco_id']), is_delete=False).values())[0]
                host_user_info = list(User.objects.filter(id=int(acco['host_user_id'])).values())[0]
                host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                             'phone': host_user_info['phone']}
                order_info = {"acco_info": acco, "host_user": host_user, "order_detail": order}
                order_list.append(order_info)
            dataset = {'u_user': {'u_user_id': u_user_id}, 'order_info': order_list}
            return common_response(code=StatusCode.SUCCESS, data=dataset)
        except:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="This user hasn't created order!",
                                   data=dataset)

    def post(self, request):
        """
        Create order
        :param request:
        :return:
        """
        u_user_id = request.user.id
        dataset = {'u_user': {'u_user_id': u_user_id}}
        order_info = request.data
        acco_id = int(order_info['acco'])
        try:
            acco_info = AccoInfo.objects.get(id=acco_id, is_delete=False)
            reserved_date = acco_info.reserved_date
            start_date = order_info['date_in']
            end_date = order_info['date_out']
            if check_date_available(acco_info.start_date, acco_info.end_date, reserved_date, start_date, end_date) is True and order_info[
                'u_people'] <= acco_info.people:
                if reserved_date == "" or reserved_date is None:
                    acco_reserve = {'reserved_date': start_date + "," + end_date}
                else:
                    acco_reserve = {'reserved_date': reserved_date + "|" + start_date + "," + end_date}
                acco_serializer = self.acco_serializer_class(instance=acco_info, data=acco_reserve, partial=True)
                acco_serializer.is_valid()
                acco_serializer.save()
                pass
            else:
                return common_response(code=StatusCode.REQUEST_VALUE_ERROR,
                                       message="The period of date_in and date_out is not available!")
            order_info['u_user'] = u_user_id
            order_serializer = self.order_serializer_class(data=order_info)
            order_serializer.is_valid()
            order_serializer.save()
            dataset['order_info'] = order_serializer.data
            host_user_info = list(User.objects.filter(id=int(acco_info.host_user_id)).values())[0]
            host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                         'phone': host_user_info['phone']}
            dataset['host_user'] = host_user
            dataset['acco_info'] = list(AccoInfo.objects.filter(id=acco_id, is_delete=False).values())[0]
            return common_response(code=StatusCode.SUCCESS, data=dataset)
        except:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect acco_id!")


@api_view(http_method_names=['GET'])
@permission_classes([IsAuthenticated, ])
def view_order(request, order_id):
    """
    view a specific order
    :param request:
    :param order_id:
    :return:
    """
    u_user_id = request.user.id
    u_user = {"u_user_id": u_user_id}
    try:
        order = list(Order.objects.filter(id=order_id, u_user=u_user_id).values())[0]
        acco = list(AccoInfo.objects.filter(id=order['acco_id'], is_delete=False).values())[0]
        host_user_info = list(User.objects.filter(id=acco['host_user_id']).values())[0]
        host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                     'phone': host_user_info['phone']}
        dataset = {"u_user": u_user, "acco_info": acco, "host_user": host_user, "order_info": order}
        return common_response(code=StatusCode.SUCCESS, data=dataset)
    except:
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect order id of this user!", data=u_user)


@api_view(http_method_names=['DELETE'])
@permission_classes([IsAuthenticated, ])
def delete_order(request, order_id):
    """
    Cancel order
    :param order_id:
    :param request:
    :return:
    """
    u_user_id = request.user.id
    try:
        order = Order.objects.get(id=order_id)
        if u_user_id == order.u_user.id:
            if datetime.now().date() < order.date_in:
                order.delete()
                return common_response(code=StatusCode.SUCCESS)
            else:
                return common_response(code=StatusCode.ERROR,
                                       message="Can not delete_order the order which date_in has past!")
        else:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Mismatched user and order!")
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect order_id!")


@api_view(http_method_names=['PUT'])
@permission_classes([IsAuthenticated, ])
def add_review_and_rate(request, order_id):
    """
    Add review and rate to an order
    :param request:
    :param order_id:
    :return:
    """
    u_user_id = request.user.id
    try:
        order = Order.objects.get(id=int(order_id), is_delete=False)
        if u_user_id != order.u_user.id:
            return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Mismatched user and order!")
        else:
            if datetime.now().date() < order.date_out:
                return common_response(code=StatusCode.ERROR, message="Date now, order does not finish!")
            order_serializer = OrderSerializer(
                instance=order, data=eval(bytes.decode(request.body)), partial=True
            )
            order_serializer.is_valid(raise_exception=True)
            order_serializer.save()

            acco = AccoInfo.objects.get(id=int(order.acco.id), is_delete=False)

            host_user_info = list(User.objects.filter(id=acco.host_user.id).values())[0]
            host_user = {'username': host_user_info['username'], 'email': host_user_info['email'],
                         'phone': host_user_info['phone']}
            orders_in_acco = Order.objects.filter(acco=int(acco.id), is_delete=False).exclude(review=None)
            acco_response = orders_in_acco.aggregate(acco_score=Avg('rate'))
            acco_response["acco_score"] = Decimal(acco_response["acco_score"]).quantize(Decimal('0.0'))
            acco_response['reviews_number'] = orders_in_acco.count()
            acco_serializer = AccoInfoSerializer(
                instance=acco, data=acco_response, partial=True
            )
            acco_serializer.is_valid()
            acco_serializer.save()
            dataset = {'u_user': u_user_id, 'host_user': host_user, 'acco_info': acco_serializer.data,
                       'order_info': order_serializer.data}
            return common_response(code=StatusCode.SUCCESS, data=dataset)
    except Exception as e:
        # print(e)
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect order_id!")
