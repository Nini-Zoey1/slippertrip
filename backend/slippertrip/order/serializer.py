from rest_framework import serializers
from .models import Order


class OrderSerializer(serializers.ModelSerializer):
    create_time = serializers.ReadOnlyField()
    update_time = serializers.ReadOnlyField()

    class Meta(object):
        model = Order
        fields = ('id', 'create_time', 'update_time', 'is_delete', 'acco', 'u_user',
                  'u_lastname', 'u_firstname', 'u_people', 'u_telephone', 'u_email', 'date_in', 'date_out',
                  'total_price', 'review', 'rate')