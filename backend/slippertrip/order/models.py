from django.db import models
from base_model.base_model import BaseModel
from django.conf import settings


class Order(BaseModel):
    """
    reserve a hotel
    """
    acco = models.ForeignKey('hotel.AccoInfo', on_delete=models.CASCADE)
    u_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    u_lastname = models.CharField(max_length=30)
    u_firstname = models.CharField(max_length=30)
    u_people = models.IntegerField()
    u_telephone = models.CharField(max_length=13)
    u_email = models.EmailField()
    date_in = models.DateField()
    date_out = models.DateField()
    total_price = models.IntegerField()
    review = models.TextField(null=True, blank=True)
    rate = models.DecimalField(max_digits=2, decimal_places=1, null=True, blank=True)

    class Meta:
        db_table = 'order'
        verbose_name = 'Order'
        verbose_name_plural = verbose_name

