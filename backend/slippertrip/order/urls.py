"""slippertrip URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from django.conf.urls import url

from .views import OrderAPIView, add_review_and_rate, delete_order, view_order

urlpatterns = [
    path('', OrderAPIView.as_view()),
    url(r'^(?P<order_id>.*)/view/', view_order),
    url(r'^(?P<order_id>.*)/rate/', add_review_and_rate),
    url(r'^(?P<order_id>.*)/delete/', delete_order),
]
