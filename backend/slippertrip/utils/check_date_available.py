
from hotel.models import AccoInfo
from .formatter import string_toDatetime, datetime_toString


def check_date_available(start_date, end_date, reserved_date, date_in_str, date_out_str):
    date_in = string_toDatetime(date_in_str).date()
    date_out = string_toDatetime(date_out_str).date()
    if start_date <= date_in and end_date >= date_out:
        if reserved_date == "" or reserved_date is None:
            return True
        dates = reserved_date.split('|')
        for date_range in dates:
            date_range = date_range.split(',')
            start = string_toDatetime(date_range[0]).date()
            end = string_toDatetime(date_range[1]).date()
            if date_in >= end or date_out <= start:
                continue
            else:
                return False
        return True
    else:
        return False
