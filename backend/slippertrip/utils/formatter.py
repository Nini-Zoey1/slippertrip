from datetime import datetime

from django.db.models import QuerySet


def data_format(data):
    """
    convert data to iteration
    :param data: single model object
    :return:
    """
    if type(data) is not QuerySet:
        data = [data]
    return data


def replace_strings(s1, s2):
    """
    replace strings
    :param s1:
    :param s2:
    :return:
    """
    res = ''
    for i in s1:
        if i == ' ':
            res += s2
        else:
            res += i
    return res


# datetime to string
def datetime_toString(dt):
    return dt.strftime("%Y-%m-%d")


# string to datetime
def string_toDatetime(string):
    return datetime.strptime(string, "%Y-%m-%d")

