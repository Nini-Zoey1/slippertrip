import requests
from bs4 import BeautifulSoup


def scrapy_currency():
    currency_dict = {}
    url = "https://www.x-rates.com/table/?from=AUD&amount=1"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    currency_list = soup.find_all('tr')[1: 11]
    for tr in currency_list:
        title = tr.find_all('td')[0].text
        currency = tr.find_all('a')[1].text
        currency_dict[title] = currency
    # print(currency_dict)
    return currency_dict


# scrapy_currency()
