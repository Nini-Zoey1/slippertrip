from django.contrib.auth import user_logged_in
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from itsdangerous import TimedJSONWebSignatureSerializer as JsonEncrypt
from itsdangerous import SignatureExpired, BadSignature
from django.core.mail import send_mail
from django.conf import settings
from rest_framework_jwt.settings import api_settings

from common_response.common_response import common_response, StatusCode
from utils.currency import scrapy_currency
from .models import User
from .serializer import UserSerializer


class SignupAPIView(APIView):
    # Allow any user (authenticated or not) to access this url
    permission_classes = (AllowAny,)

    def post(self, request):
        """
        create user, send active email
        :param request: username, email, password, phone
        :return: success message
        """
        try:
            user = request.data
            serializer = UserSerializer(data=user)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # send active email
            email = user["email"]
            send_active_email(email)
            return common_response(code=StatusCode.SUCCESS, message="Create and send active email success!")
        except Exception as e:
            exc = {"error": str(e)}
            return common_response(code=StatusCode.ERROR, message=exc)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def login(request):
    """
    user login and obtain token
    :param request: email, password
    :return: user_details (-username -token)
    """
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    try:
        email = request.data['email']
        password = request.data['password']

        user = User.objects.get(email=email, password=password)
        if user:
            # re-send active email
            if user.is_active is False:
                print(user.is_active)
                send_active_email(email)
                return common_response(code=StatusCode.IS_NOT_ACTIVE,
                                       message="User not active, we have re-sent an active email!")
            try:
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                user_details = {'username': user.username, 'token': token}
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return common_response(code=StatusCode.SUCCESS, data=user_details)
            except:
                return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Incorrect login info!")
        else:
            return common_response(code=StatusCode.HTTP_ERROR,
                                   message="403 Forbidden: can not authenticate with the given credentials or the account has been deactivated!")
    except:
        return common_response(code=StatusCode.REQUEST_VALUE_ERROR, message="Please provide a email and a password!")


@api_view(['GET'])
@permission_classes([AllowAny, ])
def active_user(request, info):
    """
    active user
    :param request: click active url(api/user/active/{encrypted user info})
    :param info: encrypted user info
    :return: success message and return to homepage
    """
    json_encrypt = JsonEncrypt(settings.SECRET_KEY, 3600)
    try:
        info = json_encrypt.loads(info)
        user = User.objects.get(email=info['confirm'])
        user.is_active = 1
        user.save()
        return redirect("http://127.0.0.1:3000/validationsuccess")
        # return common_response(code=StatusCode.SUCCESS, message='Active success!')

    except (SignatureExpired, BadSignature)as e:
        # invalid active link
        return HttpResponse('Invalid active link, please login again and we will re-send an active email!')


class UserInfoAPIView(RetrieveUpdateAPIView):
    """
    Get and Update user info
    """
    # Allow only authenticated users to access this url
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        """
        Get user info
        :param request: token
        :param args:
        :param kwargs:
        :return: user info
        """
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)
        return common_response(code=StatusCode.SUCCESS, data=serializer.data)

    def put(self, request, *args, **kwargs):
        """
        Update user info
        :param request: token, one or more or full details of user info
        :param args:
        :param kwargs:
        :return: user info
        """
        serializer_data = request.data.get('user', {})

        serializer = UserSerializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return common_response(code=StatusCode.SUCCESS, data=serializer.data)


def send_active_email(email):
    """
    send active email to user
    :param email: email
    :return: success message
    """
    # encrypt user info, send link with token by email to active
    json_encrypt = JsonEncrypt(settings.SECRET_KEY, 3600)
    info = json_encrypt.dumps({'confirm': email}).decode()

    # send email
    subject = "Slipper Trip active link"
    message = ""
    html_template = loader.get_template('active_email_template.html')
    content = {
        "email": email,
        "info": info
    }
    html_message = html_template.render(content)
    sender = settings.EMAIL_HOST_USER
    receiver = [email]
    send_mail(subject=subject, message=message, from_email=sender, recipient_list=receiver,
              html_message=html_message)


@api_view(['GET'])
@permission_classes([AllowAny, ])
def get_currency(request):
    try:
        currency = scrapy_currency()
        return common_response(code=StatusCode.SUCCESS, data=currency)
    except:
        return common_response(code=StatusCode.ERROR, message="Can not change to other currency now!")
