from __future__ import unicode_literals

from django.db import transaction
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from rest_framework.authtoken.models import Token

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager
)


class UserManager(BaseUserManager):

    def _create_user(self, email, username, phone, password, **extra_fields):
        """
        Creates and saves a User with the given email, username, phone and password.
        """
        if not all([email, phone, username]):
            raise ValueError('The given email must be set')
        try:
            with transaction.atomic():
                user = self.model(email=email, username=username, phone=phone, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except Exception:
            raise

    def create_user(self, email, username, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email,  username, phone, password,**extra_fields)

    def create_superuser(self, email, username, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(email, username, phone, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    email = models.EmailField(max_length=40, unique=True)
    username = models.CharField(max_length=30, unique=True)
    phone = models.CharField(max_length=13)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'phone']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self


class SystemConfig(models.Model):
    """
    System configs
    """
    config_name = models.CharField(max_length=20, verbose_name='Config name')
    config_index = models.CharField(max_length=20, verbose_name='Config index')

    class Meta:
        unique_together = ('config_name', 'config_index',)
        db_table = 'sys_config'
        verbose_name = 'System config'
        verbose_name_plural = verbose_name
