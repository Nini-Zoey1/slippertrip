import datetime
from django.http import JsonResponse


class StatusCode(object):
    """
    StatusCode: (code, message)
    """
    SUCCESS = (00000, 'Success!')
    ERROR = (10000, 'Error occurred!')

    # value errors
    REQUEST_VALUE_ERROR = (10001, 'Missing data integrity!')
    MODEL_VALUE_ERROR = (10002, 'Model value error!')

    # database errors
    DB_ADD_ERROR = (20001, 'DB add error!')
    DB_DELETE_ERROR = (20002, 'DB delete_order error!')
    DB_UPDATE_ERROR = (20003, 'DB update error!')
    DB_SELECT_ERROR = (20004, 'DB select error!')

    # http errors
    HTTP_ERROR = (30000, 'Http error!')

    # user status errors
    IS_NOT_ACTIVE = (40001, "User is not active, have re-sent active email, please check mailbox!")


def common_response(code, message=None, data=None, kwargs=None):
    """
    :param code: from StatusCode
    :param message: success message, string
    :param data: response data content
    :param kwargs:
    :return: custom response type with data content
    """
    if message is None:
        message = code[1]
    json_dict = {'code': code[0], 'message': message, 'data': data}
    if kwargs and isinstance(kwargs, dict) and kwargs.keys():
        json_dict.update(kwargs)
    return JsonResponse(json_dict)
