"""
using celery
"""

import django
import os

from celery import Celery
from django.conf import settings
# celery instance
from django.core.mail import send_mail

# django initial
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "proj_slippertrip.settings")  # project_name 项目名称
django.setup()

# app initial
app = Celery('celery_tasks.tasks', broker='redis://127.0.0.1:6379/8')


# start celery
# sudo redis-server /etc/redis/redis.conf
# celery -A celery_tasks.tasks worker -l info

# task: send email
@app.task
def send_register_active_email(to_email, username, token):
    """
    send register active email
    :param to_email:
    :param username:
    :param token:
    :return:
    """
    subject = "Slipper Trip active link"
    message = ""
    html_message = "<h1>Hi %s, welcome to join Slipper Trip!</h1>" \
                   "Please click follow link to active your account in 1 hour: <br/>" \
                   "<a href='http://127.0.0.1:8000/api/user/active/%s'>'http://127.0.0.1:8000/api/user/active/%s'</a" \
                   "><br/>If the link is invalid, please return to our website, " \
                   "sign in and then, we will re-sent an active email to you! <br/>" % (
                       username, token, token)

    sender = settings.EMAIL_HOST_USER
    print(sender)
    receiver = [to_email]
    print(receiver)
    # send_mail(subject, message, sender, receiver, html_message=html_message)
    send_mail(subject=subject, message=message, from_email=sender, recipient_list=receiver, html_message=html_message)
