import json
import csv
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.decomposition import PCA
from joblib import dump, load
from pandas import DataFrame
import sklearn.metrics as metrics
from sklearn import mixture
import warnings
warnings.filterwarnings('ignore')
import time



def pca_results(good_data, pca):
    # Dimension indexing
    dimensions = ['Dimension {}'.format(i) for i in range(1, len(pca.components_)+1)]

    # PCA components
    components = pd.DataFrame(np.round(pca.components_, 4), columns = good_data.keys())
    components.index = dimensions

    # PCA explained variance
    ratios = pca.explained_variance_ratio_.reshape(len(pca.components_), 1)
    variance_ratios = pd.DataFrame(np.round(ratios, 4), columns = ['Explained Variance'])
    variance_ratios.index = dimensions

    # Return a concatenated DataFrame
    return pd.concat([variance_ratios, components], axis=1)


def gmm(reduced_data, pca_samples):
    # set initial number of clusters from 2 to 50
    components = np.arange(2, 50)
    from sklearn import mixture
    # silhouette score is used to test the correctness of clustering
    max_silhouette_score = 0
    chosen_num_components = 0
    for num_components in components:
        clusterer = mixture.GaussianMixture(n_components=num_components, random_state=42).fit(reduced_data)
        preds = clusterer.predict(reduced_data)
        # Gaussian mixture model is sensitive for the initial prior distribution
        centers = clusterer.means_
        sample_preds = clusterer.predict(pca_samples)
        from sklearn.metrics import silhouette_score

        score = silhouette_score(reduced_data, preds)
        # print("The silhouette score for {} clusters is {}.".format(num_components, score))
        # select the best clustering of our model
        if max_silhouette_score < score:
            max_silhouette_score = score
            chosen_num_components = num_components
    if (chosen_num_components != components[-1]):
        clusterer = mixture.GaussianMixture(n_components=chosen_num_components, random_state=42).fit(reduced_data)
        # em algorithm is build in the model
        preds = clusterer.predict(reduced_data)
        centers = clusterer.means_
        sample_preds = clusterer.predict(pca_samples)
        score = max_silhouette_score

    # Display the predictions
    id_list = []
    cluster_list = []
    for i, pred in enumerate(preds):
        # print("id", i, "predicted to be in Cluster", pred)
        id_list.append(i)
        cluster_list.append(pred)

    return cluster_list


def algorithem():
    filename = "./machine_learning/data1.csv"
    df11 = pd.read_csv(filename)
    new_df = df11[['accommodates', 'availability_365', 'reviews_per_month', 'reviews_number']]
    new_df = new_df.dropna(subset=['accommodates', 'availability_365', 'reviews_per_month', 'reviews_number'])
    indices = [2, 39, 56, 79]

    # Create a DataFrame of the chosen samples
    samples = pd.DataFrame(new_df.loc[indices], columns=new_df.keys()).reset_index(drop=True)
    log_data = np.log(new_df)
    log_samples = np.log(samples)

    pca = PCA(n_components=new_df.shape[1])
    pca.fit(new_df)
    pca = PCA(n_components=2)
    pca.fit(new_df)
    reduced_data = pca.transform(new_df)
    pca_samples = pca.transform(log_samples)

    # Create a DataFrame for the reduced data
    reduced_data = pd.DataFrame(reduced_data, columns=['Dimension 1', 'Dimension 2'])
    cluster_list = gmm(reduced_data, pca_samples)
    df11['cluster'] = pd.DataFrame(cluster_list)
    return df11


def similarity(json_data):
    model_data = load("./machine_learning/model.save")
    # print(model_data)
    #model_data = algorithem()
    id = json_data['id']
    cluster = json_data['cluster']
    street = json_data['addr_city']


    if street == 'Zetland':
        addr = model_data.loc[model_data['addr_city'] == 'Zetland']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]

    elif street == 'Kingsford':
        addr = model_data.loc[model_data['addr_city'] == 'Kingsford']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]

    elif street == 'Randwick':
        addr = model_data.loc[model_data['addr_city'] == 'Randwick']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]

    elif street == 'Alexandria':
        addr = model_data.loc[model_data['addr_city'] == 'Alexandria']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]

    elif street == 'Bondi':
        addr = model_data.loc[model_data['addr_city'] == 'Bondi']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]


    elif street == 'Bondi Beach':
        addr = model_data.loc[model_data['addr_city'] == 'Bondi Beach']
        addr = addr.loc[addr['cluster'] == cluster]
        addr_1 = addr[['id', 'people', 'price', 'acco_score', 'reviews_number', 'beds', 'bedrooms', 'bathrooms', 'air_conditioner',
                    'tv', 'wifi', 'kitchen', 'shampoo', 'self_check_in', 'hair_dryer', 'freezer', 'accommodates',
                     'reviews_per_month', 'availability_365', 'minimum_nights', 'maximum_nights',
                     'calculated_host_listings_count'
                    ]]
        index = addr_1[addr_1['id'] == id].index.item()
        sim = cosine_similarity(addr_1)
        index_1 = addr_1.reset_index()
        index_2 = index_1[index_1['id'] == id].index.item()
        sim_1 = sim[index_2]
        list_1 = sim_1[:300]
        list_2 = []
        list_3 = []
        for i, v in enumerate(list_1):
            list_2.append(i)
            list_3.append(v)

        index_df = pd.DataFrame({'index': list_2, 'sim': list_3})
        index_df = index_df.sort_values("sim", ascending=False)
        rr_df = index_df.loc[index_df['sim'] >= 0.9]
        rr_df = rr_df['index']
        list_4 = []
        for row in rr_df:
            list_4.append(row)
        test = addr.reset_index()
        result_sim = test.iloc[list_4]


    # only run for the first time
    # dump(model_data, "model.save")

    return result_sim


def read_csv(file, json_file):
    csv_rows = []
    with open(file) as csv_file:
        reader = csv.DictReader(csv_file)
        field = reader.fieldnames
        for row in reader:
            csv_rows.extend([{field[i]:row[field[i]] for i in range(len(field))}])
        convert_write_json(csv_rows, json_file)


def convert_write_json(data, json_file):
    with open(json_file, "w") as f:
        f.write(json.dumps(data, sort_keys=False, indent=4, separators=(',', ': ')))


def read_json(json_file):
    with open(json_file, "r") as f:
        data = f.read()
    obj = json.loads(data)

    return obj


def final(json_data):
    model = similarity(json_data)
    # print(model)
    dataframe_1 = pd.DataFrame(model)
    dataframe_1.to_csv('./machine_learning/room.csv', index=False, sep=',')
    file = './machine_learning/room.csv'
    json_file = './machine_learning/room.json'
    read_csv(file, json_file)
    # intend to output a json dict
    result_1 = read_json(json_file)
    return result_1


json_data = {'id': 2020, 'addr_city': 'Bondi Beach', 'cluster': 4}
result = final(json_data)

# print(result)
# print(len(result))


