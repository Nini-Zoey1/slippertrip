from django.db import models
from django.utils import timezone


class BaseModel(models.Model):
    create_time = models.DateTimeField(default=timezone.now, verbose_name='Create Time')
    update_time = models.DateTimeField(auto_now=True, verbose_name='Update Time')
    is_delete = models.BooleanField(default=False, verbose_name='is delete_order')

    class Meta:
        abstract = True
