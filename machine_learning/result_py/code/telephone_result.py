import pandas as pd
import re
import csv

filename = "dataset.csv"
df = pd.read_csv(filename)
# print(df['amenities'])
word = ['TV', 'Wifi', 'Free street parking', 'Shampoo', 'Air conditioning', 'Hair dryer', 'Kitchen', 'Self check-in']

result_list = ['Shampoo']
for row in df['amenities']:
    result = re.search('Shampoo', row)
    if result:
        result_list.append("1")
    else:
        result_list.append('0')


with open('Shampoo result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list:
        writer.writerow(val)


result_list_1 = ['Air conditioning']
for row in df['amenities']:
    result = re.search('Air conditioning', row)
    if result:
        result_list_1.append("1")
    else:
        result_list_1.append('0')


with open('Air result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_1:
        writer.writerow(val)


result_list_2 = ['Hair dryer']
for row in df['amenities']:
    result = re.search('Hair dryer', row)
    if result:
        result_list_2.append("1")
    else:
        result_list_2.append('0')


with open('Hair result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_2:
        writer.writerow(val)


result_list_3 = ['Kitchen']
for row in df['amenities']:
    result = re.search('Kitchen', row)
    if result:
        result_list_3.append("1")
    else:
        result_list_3.append('0')


with open('Kitchen result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_3:
        writer.writerow(val)


result_list_4 = ['Self check-in']
for row in df['amenities']:
    result = re.search('Self check-in', row)
    if result:
        result_list_4.append("1")
    else:
        result_list_4.append('0')


with open('self result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_4:
        writer.writerow(val)