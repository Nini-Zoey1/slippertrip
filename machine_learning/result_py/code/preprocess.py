import re
import csv
import pandas as pd


filename = "dataset.csv"
df = pd.read_csv(filename)
#print(df.isnull().sum())

df = df.dropna(subset=['name', 'host_is_superhost', 'zipcode', 'bedrooms', 'bathrooms',
                       'beds', 'first_review', 'last_review', 'review_scores_accuracy',
                       'review_scores_cleanliness', 'review_scores_checkin',
                       'review_scores_location', 'review_scores_value'])
print(df.isnull().sum())


