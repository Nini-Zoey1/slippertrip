import pandas as pd
import re
import csv

filename = "dataset.csv"
df = pd.read_csv(filename)
word = ['TV', 'Wifi', 'Free street parking', 'Shampoo', 'Air conditioning', 'Hair dryer', 'Kitchen', 'Self check-in']

result_list = ['TV']
for row in df['amenities']:
    result = re.search('TV', row)
    if result:
        result_list.append("1")
    else:
        result_list.append('0')


with open('TV result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list:
        writer.writerow(val)

result_list_1 = ['host']
for row in df['host_is_superhost']:
    if row == 'f':
        result_list_1.append("0")
    else:
        result_list_1.append('1')

with open('host.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_1:
        writer.writerow(val)


property_list = []
for row in df['property_type']:
    property_list.append(row)

v1_list = list(set(property_list))
v1_list.sort(key=property_list.index)

result_list_2 = []
for row in df['property_type']:
    for i in range(len(v1_list)):
        if row == v1_list[i]:
            result_list_2.append(i)

dataframe = pd.DataFrame({'property_type': result_list_2})
dataframe.to_csv('property.csv', index=False, sep=',')


room_list = []
for row in df['room_type']:
    room_list.append(row)
v2_list = list(set(room_list))
v2_list.sort(key=room_list.index)
result_list_3 = []
for row in df['room_type']:
    for i in range(len(v2_list)):
        if row == v2_list[i]:
            result_list_3.append(i)
dataframe_1 = pd.DataFrame({'room_type': result_list_3})
dataframe_1.to_csv('room.csv', index=False, sep=',')


bed_list = []
for row in df['bed_type']:
    bed_list.append(row)
v3_list = list(set(bed_list))
v3_list.sort(key=bed_list.index)
result_list_4 = []
for row in df['bed_type']:
    for i in range(len(v3_list)):
        if row == v3_list[i]:
            result_list_4.append(i)
dataframe_2 = pd.DataFrame({'bed_type': result_list_4})
dataframe_2.to_csv('bed.csv', index=False, sep=',')

reshape = []
for row in df['amenities']:
    row = row.strip("{")
    list1 = row.split(",")
    reshape.append(list1[:3])
dataframe_3 = pd.DataFrame({'three_print': reshape})
dataframe_3.to_csv('first_three.csv', index=False, sep=',')
