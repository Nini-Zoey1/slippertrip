import pandas as pd
import re
import csv

filename = "dataset.csv"
df = pd.read_csv(filename)

result_list = ['Wifi']
for row in df['amenities']:
    result = re.search('Wifi', row)
    if result:
        result_list.append("1")
    else:
        result_list.append('0')


with open('Wifi result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list:
        writer.writerow(val)

result_list_1 = ['Free street parking']
for row in df['amenities']:
    result = re.search('Free street parking', row)
    if result:
        result_list_1.append("1")
    else:
        result_list_1.append('0')

with open('Parking result.csv', 'w') as f:
    writer = csv.writer(f)
    for val in result_list_1:
        writer.writerow(val)