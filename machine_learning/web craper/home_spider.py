# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

import Scrapy

class home_spider(Scrapy.spider):
    name = 'home_to_go'
    start_urls = ['https://www.hometogo.com.au/sydney/']
